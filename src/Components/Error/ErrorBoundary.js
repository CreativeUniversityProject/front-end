import React, { Component, useContext, useState } from "react";
import Err from "./Err";


export default class ErrorBoundary extends React.Component {
   state = {
    hasError: false
  };

  static getDerivedStateFromError = error => {
      console.log(error)
    return { hasError: true };
  };

 

  render() {
    const { hasError} = this.state;
    const { children } = this.props;

    return hasError ? <Err/> : children;
  }

        }