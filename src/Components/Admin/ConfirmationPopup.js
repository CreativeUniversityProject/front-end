import React from "react";
import {
  Button,
  Dialog,
  Grid,
  DialogContent,
  DialogTitle,
} from "@material-ui/core";

export default function ConfirmationPopup(props) {
  const { openPopup, setOpenPopup } = props;
  const { setDecision } = props;

  const handleSubmit = () => {
    setDecision(true);
    setOpenPopup(false);
  };

  return (
    <Dialog open={openPopup}>
      <DialogTitle>
        <div>
          <Grid item xs={12} sm={12}>
            <p>Are you sure you want to delete this user? </p>
          </Grid>
        </div>
      </DialogTitle>
      <DialogContent>
        <Button
          style={{ marginLeft: "2rem" }}
          color="secondary"
          onClick={handleSubmit}
        >
          <h6>Confirm</h6>
        </Button>
        <Button
          style={{ marginLeft: "10rem" }}
          color="primary"
          onClick={() => setOpenPopup(false)}
        >
          <h6>Cancel</h6>
        </Button>
      </DialogContent>
    </Dialog>
  );
}
