import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import { makeStyles } from "@material-ui/core/styles";
import { Paper } from "@material-ui/core";
import {
  getEmployeeBankAccount,
  updateEmployeeBankAccount,
} from "../../services/user";
import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/core";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";



const override = css`
  position: absolute;
  top: calc(50% + 10em);
  left: calc(50% - 4em);
  width: 7em;
  height: 7em;
  border: 1.1em solid rgba(252, 28, 20, 0.2);
  border-left: 1.1em solid #f50800;
  border-radius: 50%;
`;

const useStyles = makeStyles((theme) => ({
  paper: {
    background: "#194350",
    color: "#fff",
    boxshadow:
      " 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08)",
    padding: "1px 50px",
    width: "100%",
  },
  Formik: {
    marginTop: "3rem",
  },
   Snackbar: {
    top: theme.spacing(9),
  },
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
export default function EditBank(props) {
  const history = useHistory();
  const classes = useStyles();

  const id = props.match.params.id;

  const [bank, setBank] = useState([]);
  const [loadingBank, setLoadingBank] = useState(true);
   const [openNoti, setOpenNoti] = useState(false);
const [isError,setIsError]=useState(false)
  useEffect(async () => {
    getEmployeeBankAccount(id)
      .then((res) => {
        setBank(res);
        console.log(res);
        setLoadingBank(false);
        console.log(bank);
      })
      .catch((err) => {
        setIsError(true)
         setOpenNoti(true);
        setLoadingBank(false);
        console.log(err);
      });
  }, []);

  const initialValues = {
    accountNo: bank.accountNo,
    bankName: bank.bankName,
    branch: bank.branch,
  };

  const onSubmit = () => {};

  const handleSubmitting = (val) => {
    setLoadingBank(true);

    const { accountNo, bankName, branch } = val;

    const newbank = {
      accountId: bank.accountId,
      accountNo,
      bankName,
      branch,
      employee: {
        employeeId: id,
      },
    };

    updateEmployeeBankAccount(newbank)
      .then((res) => {
        setLoadingBank(false);
        history.push(`/admin/view-user/${id}`);
      })
      .catch((err) => {
        setIsError(true)
         setOpenNoti(true);
        setLoadingBank(false);
        console.log(err);
      });
  };
const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (
    <div className="container">
       {isError &&<Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        > <Alert severity="error"         onClose={handleClose1}>Something Has Happened While Updating Bank Account Details. Try Again</Alert></Snackbar>}
      <div className="row">
        <div className="card col-md-10 offset-md-2 offset-md-2">
          <Paper className={classes.paper}>
            <h3 className="text-center" style={{ fontWeight: "bold" }}>
              Edit Bank
            </h3>
          </Paper>

          <div className="card-body">
            {loadingBank && (
              <ClipLoader css={override} loading={loadingBank} size={150} />
            )}
            {!loadingBank && (
              <Formik initialValues={initialValues} onSubmit={onSubmit}>
                {({ values }) => (
                  <Form style={{ display: "flex", flexDirection: "column" }}>
                    <div className="col-6">
                      <label
                        className="form-label"
                        style={{ fontWeight: "bold" }}
                      >
                        Bank
                      </label>
                      <Field
                        name="bankName"
                        type="text"
                        placeholder="Bank"
                        className="form-control"
                      />
                    </div>
                    <br />
                    <div className="col-6">
                      <label
                        className="form-label"
                        style={{ fontWeight: "bold" }}
                      >
                        Branch
                      </label>
                      <Field
                        name="branch"
                        type="text"
                        placeholder="Branch Name"
                        className="form-control"
                      />
                    </div>
                    <br />
                    <div className="col-6">
                      <label
                        className="form-label"
                        style={{ fontWeight: "bold" }}
                      >
                        Account number
                      </label>
                      <Field
                        name="accountNo"
                        type="text"
                        placeholder="Account number"
                        className="form-control"
                      />
                    </div>
<br/>
                    <div className="col-12">
                      <button
                        className="btn btn-primary"
                        type="button"
                        onClick={() => handleSubmitting(values)}
                        style={{ fontWeight: "bold" }}
                      >
                        Update
                      </button>
                      <Link to={`/admin/view-user/${id}`}>
                        <button
                          className="btn btn-danger"
                          style={{ marginLeft: "5.0rem", fontWeight: "bold" }}
                        >
                          Cancel
                        </button>{" "}
                      </Link>
                    </div>
                  </Form>
                )}
              </Formik>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
