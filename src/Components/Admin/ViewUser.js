import React, { useContext, useEffect, useState } from "react";
import { AdminContext } from "../../context/adminContext";
import { Link, useHistory } from "react-router-dom";
import styled from "styled-components";
import pro from "../../assets/pro.jpg";
import {
  getDependentForEmployee,
  getEmployeeBankAccount,
} from "../../services/user";
import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/core";
import { makeStyles } from "@material-ui/core/styles";
import { Paper } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import { getProfileForEmployee } from "../../services/profilePicture";

const useStyles = makeStyles((theme) => ({
  Snackbar: {
    top: theme.spacing(9),
  },
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const override = css`
  position: absolute;
  top: calc(50% + 25em);
  left: calc(50% - 4em);
  width: 7em;
  height: 7em;
  border: 1.1em solid rgba(252, 28, 20, 0.2);
  border-left: 1.1em solid #f50800;
  border-radius: 50%;
`;

const ViewUser = (props) => {
  const { employees } = useContext(AdminContext);
  const [dependents, setDependents] = useState([]);
  const id = props.match.params.id;
  const employee = employees.find((employee) => employee.employeeId == id);
  const [loadingDependent, setLoadingDependent] = useState(true);
  const [loadingBank, setLoadingBank] = useState(true);
  // const [loadingProfile, setLoadingProfile] = useState(true);
  // const [profAvailable, setProfileAvailable] = useState(false);
  const [bank, setBank] = useState(null);
  const [isError, setIsError] = useState(false);
  const [openNoti, setOpenNoti] = useState(false);
  //const [profilepic, setProfilepic] = useState("");
  const {
    employeeId,
    firstName,
    lastName,
    email,
    phoneNo,
    datetime,
    address,
    gender,
    nicNo,
  } = employee;
  const { department } = employee.department;
  const { designation } = employee.designation;
  const { roleName } = employee.userRole;
  const classes = useStyles();
  const history = useHistory();
  const editHandler = () => {
    history.push(`/admin/edit-user/${id}`);
  };

  const editBankHandler = () => {
    history.push(`/admin/edit-bank/${id}`);
  };

  const editDependentHandler = () => {
    history.push(`/admin/edit-dependent/${dependents.dependentId}`);
  };

  const capitalizeFirst = (s) => {
    return s[0].toUpperCase() + s.slice(1);
  };

  useEffect(async () => {
    getDependentForEmployee(id)
      .then((res) => {
        setDependents(res);
        setLoadingDependent(false);
      })
      .catch((err) => {
        setIsError(true);
        setOpenNoti(true);
        console.log(err);
        setLoadingDependent(false);
      });

    getEmployeeBankAccount(id)
      .then((res) => {
        setBank(res);
        setLoadingBank(false);
      })
      .catch((err) => {
        setIsError(true);
        setOpenNoti(true);
        console.log(err);
        setLoadingBank(false);
      });

    // getProfileForEmployee(id)
    //   .then((res) => {
    //     setProfilepic(res.profilePicture);
    //     setLoadingProfile(false);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
  }, []);
  const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (
    <Styles>
      <Link to={"/admin"}>
        <i className="fas fa-arrow-circle-left back"></i>
      </Link>

      <div class="container">
        {isError && (
          <Snackbar
            className={classes.Snackbar}
            anchorOrigin={{ vertical: "top", horizontal: "right" }}
            open={openNoti}
            autoHideDuration={3000}
            onClose={handleClose1}
          >
            <Alert severity="error" onClose={handleClose1}>
              Something Has Happened While Fetching Data. Try Again
            </Alert>
          </Snackbar>
        )}
        <div className="back1">
          <i
            className="fas fa-user-edit text-danger edit"
            onClick={editHandler}
          ></i>

          <div className="row">
            {/* <div class="col-4">
              {!loadingProfile && (
                <img
                  src={profilepic}
                  alt=""
                  style={{
                    boxShadow:
                      "0 4px 8px 0 rgba(0, 0, 0, 0.2),0 3px 10px 0 rgba(0, 0, 0, 0.19)",
                  }}
                  width={250}
                  heigh={250}
                />
              )}
            </div> */}

            <div class="col-8">
              <h3 style={{ fontWeight: "bold" }}>
                {capitalizeFirst(firstName)} {capitalizeFirst(lastName)}
              </h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6"> Employee ID :</div>
            <div class="col-md-6">{employeeId}</div>
          </div>
          <div class="row">
            <div class="col-md-6"> Gender : </div>
            <div class="col-md-6">{gender}</div>
          </div>
          <div class="row">
            <div class="col-md-6"> NIC : </div>
            <div class="col-md-6">{nicNo}</div>
          </div>
          <div class="row">
            <div class="col-md-6"> Role :</div>
            <div class="col-md-6">{roleName}</div>
          </div>
          <div class="row">
            <div class="col-md-6"> Email :</div>
            <div class="col-md-6">{email}</div>
          </div>
          <div class="row">
            <div class="col-md-6"> Phone number :</div>
            <div class="col-md-6">{phoneNo}</div>
          </div>
          <div class="row">
            <div class="col-md-6"> Date of birth :</div>
            <div class="col-md-6">{datetime}</div>
          </div>
          <div class="row">
            <div class="col-md-6"> Address :</div>
            <div class="col-md-6">{address}</div>
          </div>
          <div class="row">
            <div class="col-md-6"> Designation :</div>
            <div class="col-md-6">{designation}</div>
          </div>
          <div class="row">
            <div class="col-md-6"> Department :</div>
            <div class="col-md-6">{department}</div>
          </div>
          <br />
        </div>
        <div class="row">
          <div class="col-4">
            {!loadingBank && (
              <h5>Bank details {bank !== null ? " " : ": none"}</h5>
            )}
          </div>
        </div>
        <br />
        <br />
        <div className="back2">
          {loadingBank && (
            <ClipLoader css={override} loading={loadingBank} size={150} />
          )}

          {!loadingBank && (
            <>
              {bank === null ? (
                <>
                  <div className="container mt-5">
                    <Link to={`/admin/add-bank/${employeeId}`}>
                      <i className="fas fa-user-plus text-danger edit"></i>
                    </Link>
                  </div>
                </>
              ) : (
                <>
                  <i
                    className="fas fa-user-edit text-danger edit"
                    onClick={editBankHandler}
                  ></i>
                  <div className="row">
                    <div class="col-md-6"> Bank :</div>
                    <div class="col-md-6">{bank.bankName}</div>
                  </div>
                  <div className="row">
                    <div class="col-md-6"> Branch :</div>
                    <div class="col-md-6">{bank.branch}</div>
                  </div>
                  <div className="row">
                    <div class="col-md-6"> Account number :</div>
                    <div class="col-md-6">{bank.accountNo}</div>
                  </div>
                </>
              )}
            </>
          )}
        </div>
        <div class="row">
          <div className="container mt-5">
            <Link to={`/admin/add-dependent/${employeeId}`}>
              <i className="fas fa-user-plus text-danger add"></i>
            </Link>
          </div>

          <div class="col-4">
            {!loadingDependent && (
              <h5>Dependents {dependents.length !== 0 ? " " : ": none"}</h5>
            )}
          </div>
        </div>

        <br />
        <br />
        <div className="back2">
          {loadingDependent && (
            <ClipLoader css={override} loading={loadingDependent} size={150} />
          )}

          {dependents.map((dependent) => {
            return (
              <>
                <Link to={`/admin/edit-dependent/${dependent.dependentId}`}>
                  <i className="fas fa-user-edit text-danger edit"></i>
                </Link>
                <div className="row">
                  <div class="col-md-6"> Dependent ID :</div>
                  <div class="col-md-6">{dependent.dependentId}</div>
                </div>
                <div className="row">
                  <div class="col-md-6"> Name :</div>
                  <div class="col-md-6">
                    {capitalizeFirst(dependent.dependentName)}
                  </div>
                </div>
                <div className="row">
                  <div class="col-md-6"> Relationship :</div>
                  <div class="col-md-6">{dependent.relationship}</div>
                </div>
                <div className="row">
                  <div class="col-md-6"> Date of birth :</div>
                  <div class="col-md-6">{dependent.dob}</div>
                </div>
                <div className="row">
                  <div class="col-md-6"> Gender :</div>
                  <div class="col-md-6">{dependent.gender}</div>
                </div>
                <br />
              </>
            );
          })}
        </div>
      </div>
    </Styles>
  );
};

const Styles = styled.div`
padding-top: 2rem;
margin-left: 0rem;

@media (min-width: 992px) {
  margin-left: 15rem;
}

  .back {
    color: red;
    font-size: 30px;
  }
  .col-md-6 {
    font-weight: bold;
    color: #000;
    padding: 1%;

  }
 
  .col-10{
   margin-left :0rem;
   margin-top:10rem;

   @media (min-width: 992px) {
    margin-left :0rem;
    margin-top: 0rem;
  }}
 
  .col-4{
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 50px 50px;
    width: 70%;
    padding: 3%;
    
    margin-left: 6rem;
    font-weight: 500;
    color: #fff;
    text-align: center;
    // background-color: #3b5998;
    background-color: #ec2329;
    font-weight: bold;

    @media (min-width: 992px) {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      margin-left:19rem;
      margin-right: auto;
      border-radius: 1.5rem;
      width: 50%;
      padding: 1%;
      font-weight: 900;
      color: #fff;
      text-align: center;
      // background-color: #3b5998;
      background-color: #ec2329;
      font-weight: bold;
    }
  }

  .edit {
    font-size: 30px;
    font-color: red;
    margin-bottom :1rem;
  }

  .add{
    font-size: 30px;
    font-color: red;
    margin-left :0rem;

    
    @media (min-width: 992px) {
      font-size: 30px;
      font-color: red;
      margin-left :7.5rem;
  
    }
  }
  

  }
  .back1{
    background-color: #fff;

    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 1.5rem;
    padding: 5%;
    margin-bottom: 5%;

    @media (min-width: 992px) {
      background-color: #fff;
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      color: #000;
      opacity: 0.8;
      border-radius: 1.5rem;
      width: 80%;
      height: 100%;
      padding: 3%;
      margin-left: 6rem;
      margin-bottom: 3%;
      margin-right: auto;
    }
  }

  .back2 {
    background-color: #fff;

    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 1.5rem;
    padding: 5%;
    margin-bottom: 5%;

    @media (min-width: 992px) {
      background-color: #fff;
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      opacity: 0.8;
      border-radius: 1.5rem;
      width: 80%;
      margin-left: 6rem;
      height: 100%;
      padding: 3%;
      margin-bottom: 3%;
    }
  }

  .back {
    margin-left: 2rem;
    padding: 1%;
    font-weight: 900;
    cursor: pointer;
  }
`;

export default ViewUser;
