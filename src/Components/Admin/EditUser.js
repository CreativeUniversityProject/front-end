import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { AdminContext } from "../../context/adminContext";
import { Formik, Field, Form, ErrorMessage } from "formik";
import Validation from "./UserValidation";
import { updateEmployee } from "../../services/user";
import { getUsers } from "../../services/user";
import { makeStyles } from "@material-ui/core/styles";
import { Paper } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

const useStyles = makeStyles((theme) => ({
  paper: {
    background: "#194350",
    color: "#fff",
    boxshadow:
      " 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08)",
    padding: "1px 50px",
    width: "100%",
  },
  Formik: {
    marginTop: "3rem",
  },
  Snackbar: {
    top: theme.spacing(9),
  },
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
export default function EditUser(props) {
  const history = useHistory();
  const classes = useStyles();

  const {
    employees,
    setEmployees,
    departments,
    designations,
    roles,
  } = useContext(AdminContext);

  const id = props.match.params.id;
  const newEmployee = employees.find((employee) => employee.employeeId == id);
  const {
    employeeId,
    firstName,
    lastName,
    email,
    phoneNo,
    datetime,
    address,
    nicNo,
    gender,
  } = newEmployee;

  const { departmentId } = newEmployee.department;
  const { designationId } = newEmployee.designation;
  const { roleId } = newEmployee.userRole;
  const [isError, setIsError] = useState(false);
     const [openNoti, setOpenNoti] = useState(false);
  const initialValues = {
    firstName,
    lastName,
    designationID: designationId,
    departmentID: departmentId,
    roleID: roleId,
    phoneNo,
    datetime: datetime,
    address: address,
    nicNo: nicNo,
    gender: gender,
  };

  const onSubmit = (fields) => {};

  const handleSubmitting = (val) => {
    const {
      firstName,
      lastName,
      roleID,
      designationID,
      departmentID,
      phoneNo,
      datetime,
      address,
      nicNo,
      gender,
    } = val;

    // let userRoleArr = [{ roleId: "51" }];
    // if (roleID !== 51) {
    //   const obj = { roleId: roleID };
    //   userRoleArr = [...userRoleArr, obj];
    // }

    const newEmp = {
      employeeId,
      datetime,
      address,
      firstName,
      lastName,
      nicNo,
      gender,
      email,
      phoneNo,
      department: {
        departmentId: departmentID,
      },
      designation: {
        designationId: designationID,
      },
      userRole: {
        roleId: roleID,
      },
      // userRole: userRoleArr,
    };

    updateEmployee(newEmp)
      .then((res) => {
        getUsers()
          .then((res) => {
            setEmployees(res);
            history.push("/admin");
          })
          .catch((err) => {
            setIsError(true);
            setOpenNoti(true);
            console.log(err);
          });
      })
      .catch((err) => {
        setIsError(true);
        setOpenNoti(true);
        console.log(err);
      });
  };
const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (
    <div className="container">
      {isError && (<Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        > 
        <Alert severity="error"  onClose={handleClose1}>
          Something Has Happened While Updating User Details. Try Again
        </Alert></Snackbar>
      )}
      <div className="row">
        <div className="card col-md-10 offset-md-2 offset-md-2">
          <Paper className={classes.paper}>
            <h3 className="text-center" style={{ fontWeight: "bold" }}>
              Edit User
            </h3>
          </Paper>
          <div className="card-body">
            <Formik
              initialValues={initialValues}
              validationSchema={Validation}
              onSubmit={onSubmit}
            >
              {({ values }) => (
                <Form style={{ display: "flex", flexDirection: "column" }}>
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      First name
                    </label>
                    <Field
                      name="firstName"
                      type="text"
                      placeholder="First Name"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="firstName"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <br />
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Last name
                    </label>
                    <Field
                      name="lastName"
                      type="text"
                      placeholder="Last Name"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="lastName"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <br />

                  <div className="col-6">
                    <label
                      class="form-check-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Gender
                    </label>
                    <br />
                    <div class="form-check form-check-inline">
                      <Field
                        class="form-check-input"
                        type="radio"
                        name="gender"
                        value="male"
                      />
                      <label class="form-check-label">Male</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <Field
                        class="form-check-input"
                        type="radio"
                        name="gender"
                        value="female"
                      />
                      <label class="form-check-label">Female</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <Field
                        class="form-check-input"
                        type="radio"
                        name="gender"
                        value="other"
                      />
                      <label class="form-check-label" for="inlineRadio3">
                        Other
                      </label>
                    </div>
                  </div>
                  <br />

                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      NIC number
                    </label>
                    <Field
                      name="nicNo"
                      type="text"
                      placeholder="NIC Number"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="nicNo"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>

                  {/*<div className="col-6">
                    <label className="form-label">Employee ID</label>
                    <Field
                      name="employeeId"
                      type="text"
                      placeholder="Employee ID"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="employeeId"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>*/}
                  <br />
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Designation
                    </label>
                    <Field
                      name="designationID"
                      component="select"
                      className="form-control"
                    >
                      <option value="" disabled selected hidden>
                        Please Choose...
                      </option>
                      {designations.map((desig) => {
                        return (
                          <option value={desig.designationId}>
                            {desig.designation}
                          </option>
                        );
                      })}
                    </Field>
                  </div>
                  <br />
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Department
                    </label>
                    <Field
                      name="departmentID"
                      component="select"
                      className="form-control"
                    >
                      <option value="" disabled selected hidden>
                        Please Choose...
                      </option>
                      {departments.map((dep) => {
                        return (
                          <option value={dep.departmentId}>
                            {dep.department}
                          </option>
                        );
                      })}
                    </Field>
                  </div>
                  <br />
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Role
                    </label>
                    <Field
                      name="roleID"
                      component="select"
                      className="form-control"
                    >
                      <option value="" disabled selected hidden>
                        Please Choose...
                      </option>
                      {roles.map((role) => {
                        return (
                          <option value={role.roleId}>{role.roleName}</option>
                        );
                      })}
                    </Field>
                  </div>
                  <br />
                  {/* <div className="col-6">
                    <label className="form-label">Email</label>
                    <Field
                      name="email"
                      type="email"
                      placeholder="Email"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="email"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div> */}
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Phone number
                    </label>
                    <Field
                      name="phoneNo"
                      type="text"
                      placeholder="phone number"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="phoneNo"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <br />
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Address
                    </label>
                    <Field
                      name="address"
                      type="text"
                      placeholder="address"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="address"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <br />
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Date of birth
                    </label>
                    <Field
                      name="datetime"
                      type="date"
                      placeholder="Birth Date"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="datetime"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <br />
                  <div className="col-12">
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={() => handleSubmitting(values)}
                      style={{ fontWeight: "bold" }}
                    >
                      Update
                    </button>
                    <Link to={"/admin"}>
                      <button
                        className="btn btn-danger"
                        style={{ marginLeft: "5.0rem", fontWeight: "bold" }}
                      >
                        Cancel
                      </button>{" "}
                    </Link>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
}
