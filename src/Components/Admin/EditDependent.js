import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import { makeStyles } from "@material-ui/core/styles";
import { Paper } from "@material-ui/core";
import { updateDependent, getDependent } from "../../services/user";
import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/core";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

const override = css`
  position: absolute;
  top: calc(50% + 10em);
  left: calc(50% - 4em);
  width: 7em;
  height: 7em;
  border: 1.1em solid rgba(252, 28, 20, 0.2);
  border-left: 1.1em solid #f50800;
  border-radius: 50%;
`;

const useStyles = makeStyles((theme) => ({
  paper: {
    background: "#194350",
    color: "#fff",
    boxshadow:
      " 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08)",
    padding: "1px 50px",
    width: "100%",
  },
  Formik: {
    marginTop: "3rem",
  },
    Snackbar: {
    top: theme.spacing(9),
  },
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
export default function EditDependent(props) {
  const history = useHistory();
  const classes = useStyles();

  const id = props.match.params.id;

  const [dependent, setDependent] = useState([]);
  const [loadingDependent, setLoadingDependent] = useState(true);
  const [isError, setIsError] = useState(false);
    const [openNoti, setOpenNoti] = useState(false);
  useEffect(async () => {
    getDependent(id)
      .then((res) => {
        setDependent(res);
        console.log(dependent);
        setLoadingDependent(false);
      })
      .catch((err) => {
        setIsError(true);
        setOpenNoti(true);
        setLoadingDependent(false);
        console.log(err);
      });
  }, []);

  const initialValues = {
    // accountNo: dependent.accountNo,
    // bankName: dependent.bankName,
    // branch: dependent.branch,
    dependentName: dependent.dependentName,
    dob: dependent.dob,
    gender: dependent.gender,
    relationship: dependent.relationship,
  };

  const onSubmit = () => {};

  const handleSubmitting = (val) => {
    setLoadingDependent(true);
    const { dependentName, relationship, gender, dob } = val;

    const editedDependent = {
      dependentName,
      relationship,
      gender,
      dob,
      dependentId: id,
      employee: {
        employeeId: dependent.employee.employeeId,
      },
    };

    updateDependent(editedDependent)
      .then((res) => {
        setLoadingDependent(false);
        history.push(`/admin/view-user/${dependent.employee.employeeId}`);
      })
      .catch((err) => {
        setIsError(true);
        setOpenNoti(true);
        console.log(err);
      });
  };
const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (
    <div className="container">
      {isError && (<Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        > 
        <Alert severity="error"    onClose={handleClose1}>
          Something Has Happened While Updating Dependent Details. Try Again
        </Alert></Snackbar>
      )}
      <div className="row">
        <div className="card col-md-10 offset-md-2 offset-md-2">
          <Paper className={classes.paper}>
            <h3 className="text-center" style={{ fontWeight: "bold" }}>
              Edit dependent
            </h3>
          </Paper>

          <div className="card-body">
            {loadingDependent && (
              <ClipLoader
                css={override}
                loading={loadingDependent}
                size={150}
              />
            )}
            {!loadingDependent && (
              <Formik initialValues={initialValues} onSubmit={onSubmit}>
                {({ values }) => (
                  <Form style={{ display: "flex", flexDirection: "column" }}>
                    <div className="col-6">
                      <label
                        className="form-label"
                        style={{ fontWeight: "bold" }}
                      >
                        Name
                      </label>
                      <Field
                        name="dependentName"
                        type="text"
                        placeholder="Dependent name"
                        className="form-control"
                      />
                    </div>
                    <br />
                    <div className="col-6">
                      <label
                        className="form-label"
                        style={{ fontWeight: "bold" }}
                      >
                        Date of birth
                      </label>
                      <Field
                        name="dob"
                        type="date"
                        placeholder="Date of Birth"
                        className="form-control"
                      />
                    </div>
                    <br />
                    <div className="col-6">
                      <div className="row">
                        <div className="col-3">
                          <label
                            className="form-label"
                            style={{ fontWeight: "bold" }}
                          >
                            Gender
                          </label>
                        </div>
                        <div className="col-2">
                          <Field
                            type="radio"
                            name="gender"
                            value="male"
                            className="form-check-input"
                          />
                          <label className="form-check-label">Male</label>
                        </div>
                        <br />
                        <div className="col-2">
                          <label className="form-check-label">
                            <Field
                              type="radio"
                              name="gender"
                              value="female"
                              className="form-check-input"
                            />
                            Female
                          </label>
                        </div>
                        <div className="col-2">
                          <label className="form-check-label">
                            <Field
                              type="radio"
                              name="gender"
                              value="other"
                              className="form-check-input"
                            />
                            Other
                          </label>
                        </div>
                      </div>
                    </div>
                    <br />

                    <div className="col-6">
                      <div className="row">
                        <div className="col-3">
                          <label
                            className="form-label"
                            style={{ fontWeight: "bold" }}
                          >
                            Relationship
                          </label>
                        </div>
                        <div className="col-2">
                          <Field
                            type="radio"
                            name="relationship"
                            value="partner"
                            className="form-check-input"
                          />
                          <label className="form-check-label">Partner</label>
                        </div>
                        <div className="col-2">
                          <label className="form-check-label">
                            <Field
                              type="radio"
                              name="relationship"
                              value="child"
                              className="form-check-input"
                            />
                            Child
                          </label>
                        </div>
                      </div>
                    </div>
                    <br />

                    <div className="col-12">
                      <button
                        className="btn btn-primary"
                        type="button"
                        onClick={() => handleSubmitting(values)}
                        style={{ fontWeight: "bold" }}
                      >
                        Update
                      </button>
                      <Link
                        to={`/admin/view-user/${dependent.employee.employeeId}`}
                      >
                        <button
                          className="btn btn-danger"
                          style={{ marginLeft: "5.0rem", fontWeight: "bold" }}
                        >
                          Cancel
                        </button>{" "}
                      </Link>
                    </div>
                  </Form>
                )}
              </Formik>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
