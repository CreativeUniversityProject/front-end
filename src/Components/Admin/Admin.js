import React, { useContext, useEffect, useState } from "react";
import { AdminContext } from "../../context/adminContext";
import styled from "styled-components";
import {
  deleteEmployee,
  getUsers,
  getDesignations,
  getDepartments,
  getRoles,
} from "../../services/user";
import { Link } from "react-router-dom";
import ConfirmationPopup from "./ConfirmationPopup";
import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/core";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import Hidden from "@material-ui/core/Hidden";
import { makeStyles } from "@material-ui/core/styles";
import { LibraryBooksRounded } from "@material-ui/icons";


const Styles = styled.div`
  //padding-top: 3rem;
  margin-left: 0rem;

  @media (min-width: 992px) {
    margin-left: 15rem;
  }

  .sI {
    color: red;
    font-size: 20px;
    margin-left: 10px;
  }

  .mI {
    color: red;
    font-size: 30px;
  }

  table {
    margin-top: 25px;
  }

  table tr {
    margin: 5px;
  }
  i{
    cursor: pointer;
  }
`;

const override = css`
  position: absolute;
  top: calc(50% + 10em);
  left: calc(50% - 4em);
  width: 9em;
  height: 9em;
  border: 1.1em solid rgba(252, 28, 20, 0.2);
  border-left: 1.1em solid #f50800;
  border-radius: 50%;
`;
const useStyles = makeStyles((theme) => ({
 
  Snackbar: {
    top: theme.spacing(9),
  },
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const Admin = () => {
  const {
    employees,
    setEmployees,
    setRoles,
    setDepartments,
    setDesignations,
  } = useContext(AdminContext);
 const classes = useStyles();
  const [openPopup, setOpenPopup] = useState(false);
  const [decision, setDecision] = useState(false);
  const [idtodlt, setIdtodlt] = useState(null);
  const [loading, setLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [isDetele, setIsDelete] = useState(false);
   const [openNoti, setOpenNoti] = useState(false);
  employees.sort((a, b) => {
    return a.employeeId - b.employeeId;
  });

  const deleteUser = (id) => {
    setOpenPopup(true);
    setIdtodlt(id);
  };

  const capitalizeFirst = (s) => {
    return s[0].toUpperCase() + s.slice(1);
  };

  useEffect(async () => {
    if (employees.length !== 0) {
      setLoading(false);
    }

    getDesignations()
      .then((res) => {
        setDesignations(res);
      })
      .catch((err) => {
        setIsError(true);
          setOpenNoti(true);
        console.log(err);
      });

    getUsers()
      .then((res) => {
        setEmployees(res);
        setLoading(false);
      })
      .catch((err) => {
        setIsError(true);
        console.log(err);
        setLoading(false);
      });

    getDepartments()
      .then((res) => {
        setDepartments(res);
      })
      .catch((err) => {
        setIsError(true);
          setOpenNoti(true);
        console.log(err);
      });

    getRoles()
      .then((res) => {
        setRoles(res);
      })
      .catch((err) => {
        setIsError(true);
          setOpenNoti(true);
        console.log(err);
      });
  }, []);

  useEffect(() => {
    if (decision) {
      deleteEmployee(idtodlt)
        .then((res) => {
          setDecision(false);
          setIdtodlt(null);
          getUsers()
            .then((res) => {
              setEmployees(res);
            })
            .catch((err) => {
              setIsDelete(true);
              setOpenNoti(true);
              console.log(err);
            });
        })
        .catch((err) => {
          setIsDelete(true);
            setOpenNoti(true);
          console.log(err);
        });
    }
  }, [decision]);
const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (
    <Styles>
      {loading && <ClipLoader css={override} loading={loading} size={150} />}
      {isError && (<Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        >
        <Alert severity="error" onClose={handleClose1}>
          Something Has Happened While Fetching Data. Try Again
        </Alert></Snackbar>
      )}
      {isDetele && (<Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        >
        <Alert severity="error" onClose={handleClose1}>
          Something Has Happened While Deleting User. Try Again
        </Alert></Snackbar>
      )}
      <div className="container mt-5">
        <Link to={"/admin/add-user"}>
          <i className="fas fa-user-plus text-danger mI"></i>
        </Link>
      </div>
      <table className="table">
        <thead>
        <Hidden smDown >
          <th scope="col">Employee Id</th>
          </Hidden>
          <th scope="col">Full Name</th>
          <Hidden smDown >
          <th scope="col">Email</th>
          </Hidden>
          <th scope="col">Role</th>
          <th scope="col">Actions</th>
        </thead>
        <tbody>
          {employees.map((emp) => {
            return (
              <tr>
                  <Hidden smDown >
                <td>{emp.employeeId}</td>
                </Hidden>
                <td>
                  {capitalizeFirst(emp.firstName)}{" "}
                  <Hidden smDown>
                  {capitalizeFirst(emp.lastName)}
                  </Hidden>
                </td>
                <Hidden smDown >
                <td>{emp.email}</td>
</Hidden>
                <td>{emp.userRole.roleName}</td>

                <td>
                  <Link to={`/admin/view-user/${emp.employeeId}`}>
                    <LibraryBooksRounded color="primary" />
                  </Link>
                  <Link to={`/admin/edit-user/${emp.employeeId}`}>
                    <i className="fas fa-user-edit text-danger sI"></i>
                  </Link>
                  <Link to={`/admin/add-dependent/${emp.employeeId}`}>
                    <i className="fas fa-child text-danger sI"></i>
                  </Link>
                  <i
                    className="fas fa-user-times text-danger sI"
                    onClick={() => deleteUser(emp.employeeId)}
                  ></i>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <ConfirmationPopup
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        decision={decision}
        setDecision={setDecision}
      ></ConfirmationPopup>
    </Styles>
  );
};

export default Admin;
