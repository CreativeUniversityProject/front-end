import {React,useState} from "react";
import { Link, useHistory } from "react-router-dom";
import { Formik, Field, Form } from "formik";
import { AppBar, Box, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { addBankAccount } from "../../services/user";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

const useStyles = makeStyles((theme) => ({
  paper: {
    background: "#194350",
    color: "#fff",
    boxshadow:
      " 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08)",
    padding: "0.01px 50px",
    width: "100%",
  },
  Formik: {
    marginTop: "3rem",
  },
  Snackbar: {
    top: theme.spacing(9),
  },
}));
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
export default function AddBank(props) {
  const history = useHistory();
  const classes = useStyles();
  const id = props.match.params.id;

  const initialValues = {
    accountNo: "",
    bankName: "",
    branch: "",
  };
const [isError, setIsError] = useState(false)
 const [openNoti, setOpenNoti] = useState(false);
  const onSubmit = () => {};

  const handleSubmitting = (val) => {
    console.log(val);
    const { accountNo, bankName, branch } = val;

    const newbank = {
      accountNo,
      bankName,
      branch,
      employee: {
        employeeId: id,
      },
    };

    addBankAccount(newbank)
      .then((res) => {
        // history.push(`/admin/view-user/${id}`);
      })
      .catch((err) => {
        setOpenNoti(true);
        setIsError(true)
        console.log(err);
      });
  };
const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (
    <div className="container">
       {isError &&<Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        > <Alert severity="error" onClose={handleClose1} >Something Has Happened While Adding Bank Account Details. Try Again</Alert></Snackbar>}
        
      <div className="row">
        <div className="card col-md-10 offset-md-2 offset-md-2">
          <Paper className={classes.paper} elevation={3}>
            <h5 className="text-center">Add Bank</h5>
          </Paper>
          <div className="card-body">
            <Formik initialValues={initialValues} onSubmit={onSubmit}>
              {({ values }) => (
                <Form style={{ display: "flex", flexDirection: "column" }}>
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Bank
                    </label>
                    <Field
                      name="bankName"
                      type="text"
                      placeholder="Bank"
                      className="form-control"
                    />
                  </div>
                  <br />
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Branch
                    </label>
                    <Field
                      name="branch"
                      type="text"
                      placeholder="Branch Name"
                      className="form-control"
                    />
                  </div>
                  <br />
                  <div className="col-6">
                    <label
                      className="form-label"
                      style={{ fontWeight: "bold" }}
                    >
                      Account number
                    </label>
                    <Field
                      name="accountNo"
                      type="text"
                      placeholder="Account number"
                      className="form-control"
                    />
                  </div>
                  <br/>
                  <div className="col-12">
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={() => handleSubmitting(values)}
                      style={{ fontWeight: "bold" }}
                    >
                      Update
                    </button>
                    <Link to={`/admin/view-user/${id}`}>
                      <button
                        className="btn btn-danger"
                        style={{ marginLeft: "5.0rem", fontWeight: "bold" }}
                      >
                        Cancel
                      </button>{" "}
                    </Link>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
}
