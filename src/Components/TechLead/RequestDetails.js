import React, { useContext, useState, useEffect } from "react";
import { RequestContext } from "../../context/techLeadContext";
import styled from "styled-components";
import { Link, useHistory } from "react-router-dom";
import { updateExpenseClaim } from "../../services/ExpenseForm";
import { updateMedicalClaim } from "../../services/MedicalForm";
import Confirmpopup from "./Confirmpopup";
import { addEmail } from "../../services/ExpenseForm";
import { getAccoutant } from "../../services/Accountant";
import { getDependent } from "../../services/user";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import { makeStyles } from "@material-ui/core/styles";
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles((theme) => ({
 Snackbar: {
    top: theme.spacing(9),
  },
}));

export default function RequestDetails(props) {
  const userDetails = JSON.parse(localStorage.getItem("user"));
  const [isError, setIsError] = useState(false);
  const [openNoti, setOpenNoti] = useState(false);
  const classes = useStyles();
  const [isSent, setIsSent] = useState(false);
  const id = props.match.params.id;
  let urlPart = props.match.path.replace(":id", "");
  const history = useHistory();
  const { requests, setRequests, medicalRequests, setMedicalRequests, Acc } =
    useContext(RequestContext);
  let type = "expense";
  let selectedRequest = {};
  selectedRequest = requests.find((request) => request.expenseFormId == id);
  if (typeof selectedRequest === "undefined") {
    type = "medical";
    selectedRequest = medicalRequests.find(
      (request) => request.medicalFormId == id
    );
  }

  const { formDescription, progressStatus, submittedDate, amount } =
    selectedRequest;
  const { firstName, lastName } = selectedRequest.employee;

  let receipt = [];
  let self = null;
  if (type === "medical") {
    receipt = selectedRequest.receipt;
    self = selectedRequest.self;
    console.log(self);
  } else {
    receipt = selectedRequest.image;
  }

  let formId = "";
  if (type === "expense") {
    formId = selectedRequest.expenseFormId;
  } else {
    formId = selectedRequest.medicalFormId;
  }

  console.log(selectedRequest);

  const [openPopup, setOpenPopup] = useState(false);
  const [act, setAct] = useState(null);
  const [decision, setDecision] = useState(false);
  const [remark, setRemark] = useState("");

  const changeStatus = (func) => {
    setOpenPopup(true);
    setAct(func);
  };

  const obj = {
    receiverEmail: selectedRequest.employee.email,
    subject: "",
    text: "",
  };

  const email = Acc.map((name) => {
    return name.email;
  });
  const newValue = email.find((email) => email != undefined);
  console.log(newValue);

  const objAccount = {
    receiverEmail: newValue,
    subject: "",
    text: "",
  };

  console.log(objAccount);

  useEffect(() => {
    if (decision) {
      let changedRequest = selectedRequest;

      if (act === 1) {
        changedRequest.progressStatus = "accepted by tech lead";
        obj.subject = "Request accepted by tech lead";
        obj.text = `The request of id ${formId} submitted on ${changedRequest.submittedDate} 
          has been accepted by techlead. the request is now being forwarded to accountant for further processing`;

        objAccount.subject = " There is a claim for review";
        objAccount.text = `Claim submitted on ${changedRequest.submittedDate} by ${changedRequest.employee.firstName} 
        has been accepted by techlead. Visit http://hexclanbucket1.s3-website.us-east-2.amazonaws.com/ for futher details
        `;
      } else {
        changedRequest.progressStatus = "rejected by tech lead";
        changedRequest.remark = remark;
        obj.subject = "Request rejected by tech lead";
        obj.text = `The request of id ${formId} submitted on ${changedRequest.submittedDate} 
        has been rejected by techlead due to ${remark}`;
      }

      if (type === "medical") {
        updateMedicalClaim(changedRequest)
          .then((res) => {
            addEmail(obj)
              .then((res) => {})
              .catch((err) => {
                console.log(err);
              });

            if (act === 1) {
              addEmail(objAccount)
                .then((res) => {})
                .catch((err) => {
                  console.log(err);
                });
            }

            const filteredItems = medicalRequests.filter(
              (request) => request.medicalFormId !== formId
            );
            const newItems = [...filteredItems, res];
            setMedicalRequests(newItems);
            setAct(null);
            setDecision(false);
            setRemark("");
            history.push("/techlead/requests");
          })
          .catch((err) => {
            setIsSent(true);
             setOpenNoti(true);
            console.log(err);
          });
      } else {
        updateExpenseClaim(changedRequest)
          .then((res) => {
            addEmail(obj)
              .then((res) => {})
              .catch((err) => {
                console.log(err);
              });

            if (act === 1) {
              addEmail(objAccount)
                .then((res) => {})
                .catch((err) => {
                  console.log(err);
                });
            }

            const filteredItems = requests.filter(
              (request) => request.expenseFormId !== formId
            );
            const newItems = [...filteredItems, res];
            setRequests(newItems);
            setAct(null);
            setDecision(false);
            setRemark("");
            history.push("/techlead/requests");
          })
          .catch((err) => {
            setIsSent(true);
             setOpenNoti(true);
            console.log(err);
          });
      }
    }
  }, [decision]);
const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (
    <Styles>
      <div className="container">
        {isError && ( <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        >
          <Alert severity="error" onClose={handleClose1}>
            Something Has Happened While Fetching Data. Try Again
          </Alert></Snackbar>
        )}
  {isSent && ( <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        >
          <Alert severity="error" onClose={handleClose1}>
            Something Has Happened While Updating Data. Try Again
          </Alert></Snackbar>
        )}
        <div className="row">
          <div className="col-2">
            <Link to="/techlead/requests">
              <button className="btn btn-danger">Go back</button>{" "}
            </Link>
          </div>
          <div className="col-6">
            <h3>Details</h3>
          </div>

          {props.match.path == "/techlead/requests/:id" ? (
            <div className="col-4">
              <i
                className="fas fa-check text-success"
                onClick={() => changeStatus(1)}
              ></i>
              <i
                className="fas fa-times text-danger"
                onClick={() => changeStatus(-1)}
              ></i>
            </div>
          ) : (
            <div></div>
          )}
        </div>

        <div className="content">
          <div className="back">
            <div className="row">
              <div className="col-md-6"> Employee </div>
              <div className="col-md-6">
                {firstName} {lastName}
              </div>
            </div>

            <div className="row">
              <div className="col-md-6"> Request No </div>
              <div className="col-md-6">{formId}</div>
            </div>
            <div className="row">
              <div className="col-md-6"> Expense Type </div>
              <div className="col-md-6">{formDescription}</div>
            </div>
            {/* <div className="row">
              <div className="col-md-6"> progressStatus </div>
              <div className="col-md-6">{progressStatus}</div>
            </div> */}
            <div className="row">
              <div className="col-md-6"> submittedDate </div>
              <div className="col-md-6">{submittedDate}</div>
            </div>
            {type === "medical" ? (
              <div className="row">
                <div className="col-md-6"> Claim for </div>
                {self === true ? (
                  <div className="col-md-6">Self</div>
                ) : (
                  <>
                    <div className="col-md-6">Dependent</div>
                  </>
                )}
              </div>
            ) : null}

            <div className="row">
              <div className="col-md-6"> Amount </div>
              <div className="col-md-6">{amount}</div>
            </div>
          </div>

          {type === "medical" ? (
            <>
              {self !== true ? (
                <div>
                  <div className="col-7">
                    <h3>Dependents</h3>
                  </div>
                  <br />
                  <br />
                  <div className="back2">
                    {selectedRequest.dependent.map((dependent) => {
                      return (
                        <>
                          <div className="row">
                            <div className="col-md-6"> Dependent ID:</div>
                            <div className="col-md-6">
                              {dependent.dependentId}
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-6"> Name:</div>
                            <div className="col-md-6">
                              {dependent.dependentName}
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-6"> Relationship:</div>
                            <div className="col-md-6">
                              {dependent.relationship}
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-6"> Date of Birth:</div>
                            <div className="col-md-6">{dependent.dob}</div>
                          </div>
                          <div className="row">
                            <div className="col-md-6"> Gender:</div>
                            <div className="col-md-6">{dependent.gender}</div>
                          </div>
                          <br />
                        </>
                      );
                    })}
                  </div>
                </div>
              ) : null}
            </>
          ) : null}

          <div>
            <div className="col-7">
              <h3>Receipts</h3>
            </div>
            <br />
            <br />

            <div className="back2">
              {type === "expense"
                ? receipt.map((rec) => {
                    return (
                      <div key={rec.imgId}>
                        <div className="row">
                          <div className="col-md-6"> Image ID : </div>
                          <div className="col-md-6">
                            {rec.imgId}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Amount : </div>
                          <div className="col-md-6">
                            {rec.amount}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Description : </div>
                          <div className="col-md-6">
                            {rec.description}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6">Receipt Image :</div>
                          <img
                            src={rec.billImage}
                            alt={rec.imgId}
                            width={250}
                            height={250}
                          />
                          <br />
                        </div>
                      </div>
                    );
                  })
                : receipt.map((rec) => {
                    return (
                      <div key={rec.receiptId}>
                        <div className="row">
                          <div className="col-md-6"> Receipt ID : </div>
                          <div className="col-md-6">
                            {rec.receiptId}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Amount : </div>
                          <div className="col-md-6">
                            {rec.amount}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Description : </div>
                          <div className="col-md-6">
                            {rec.description}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6">Receipt Image : </div>
                          <div className="im">
                            <img
                              src={rec.receiptImage}
                              alt={rec.receiptId}
                              width={250}
                              height={250}
                            />
                            <br />
                          </div>
                        </div>
                      </div>
                    );
                  })}
            </div>
          </div>
          <Confirmpopup
            openPopup={openPopup}
            setOpenPopup={setOpenPopup}
            act={act}
            setAct={setAct}
            decision={decision}
            setDecision={setDecision}
            remark={remark}
            setRemark={setRemark}
          ></Confirmpopup>
        </div>
      </div>
    </Styles>
  );
}

const Styles = styled.div`
  padding-top: 3rem;
  margin-left: 0.4rem;

  i {
    font-size: 2rem;
    margin-left: 3rem;
  }

  @media (min-width: 992px) {
    margin-left: 250px;
  }

  .content {
    margin-top: 3rem;
    margin-left: 1rem;

    @media (min-width: 992px) {
      margin-top: 3rem;
      margin-left: 3rem;
    }
  }

  .col-md-6 {
    font-weight: 600;
  }

  .row {
    margin-bottom: 1rem;
  }
  .btn-danger {
    padding-right: 3rem;
    width: 100%;

    font-weight: 500;
    background-color: #ec2329;
    cursor: pointer;

    @media (min-width: 992px) {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      border-radius: 25px 50px;
      width: 50%;
      padding: 5%;
      font-weight: 900;
      background-color: #ec2329;
      cursor: pointer;
    }
  }
  .col-6 {
    border-radius: 50px 50px;
    width: 50%;
    padding: 3%;
    margin-left: 2rem;
    font-weight: 500;
    color: #fff;
    text-align: center;
    background-color: #ec2329;

    @media (min-width: 992px) {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      margin-left: auto;
      margin-right: auto;
      border-radius: 50px 50px;
      width: 50%;
      padding: 1%;
      font-weight: 900;
      color: #fff;
      text-align: center;
      // background-color: #3b5998;
      background-color: #ec2329;
      font-weight: bold;
    }
  }
  .col-7 {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 50px 50px;
    width: 70%;
    padding: 3%;
    margin-left: 4rem;
    font-weight: 500;
    color: #fff;
    text-align: center;
    // background-color: #3b5998;
    background-color: #ec2329;
    font-weight: bold;

    @media (min-width: 992px) {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      margin-left: 7rem;
      margin-right: auto;
      border-radius: 1.5rem;
      width: 50%;
      padding: 1%;
      font-weight: 900;
      color: #fff;
      text-align: center;
      // background-color: #3b5998;
      background-color: #ec2329;

      font-weight: bold;
    }
  }

  .col-2 {
    font-weight: bold;
    color: #000;
  }
  .col-10 {
    font-weight: bold;
    font-weight: 900;
    color: #000;
  }
  .im {
    padding: 1%;
    margin-bottom: 3%;
    border-radius: 8px;
  }
  .back {
    background-color: #fff;

    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 1.5rem;
    padding: 5%;
    margin-bottom: 5%;

    @media (min-width: 992px) {
      background-color: #fff;
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      color: #000;
      opacity: 0.8;
      border-radius: 1.5rem;
      width: 80%;
      height: 100%;
      padding: 3%;
      margin-bottom: 3%;
      margin-right: auto;
    }
  }
  .back2 {
    background-color: #fff;

    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 1.5rem;
    padding: 5%;
    margin-bottom: 5%;

    @media (min-width: 992px) {
      background-color: #fff;
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      opacity: 0.8;
      border-radius: 1.5rem;
      width: 80%;
      height: 100%;
      padding: 3%;
      margin-bottom: 3%;
    }
  }
`;
