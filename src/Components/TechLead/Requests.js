import React, { useContext, useState, useEffect } from "react";
import { RequestContext } from "../../context/techLeadContext";
import styled from "styled-components";
import {
  updateExpenseClaim,
  getExpenseClaims,
} from "../../services/ExpenseForm";
import {
  updateMedicalClaim,
  getMedicalClaims,
} from "../../services/MedicalForm";
import Confirmpopup from "./Confirmpopup";
import ReqTable from "./ReqTable";
import { addEmail } from "../../services/ExpenseForm";
import { getAccoutant } from "../../services/Accountant";
import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/core";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import { makeStyles } from "@material-ui/core/styles";
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles((theme) => ({
 Snackbar: {
    top: theme.spacing(9),
  },
}));


const Styles = styled.div`
  padding-top: 1rem;
  margin-left: 0.4rem;

  i {
    margin: 2.5px;
  }

  @media (min-width: 992px) {
    margin-left: 250px;

    i {
      margin: 2px;
    }
  }

  > .medical {
    margin-top: 3rem;
  }
`;

const override = css`
  position: absolute;
  top: calc(50% + 10em);
  left: calc(50% - 4em);
  width: 9em;
  height: 9em;
  border: 1.1em solid rgba(252, 28, 20, 0.2);
  border-left: 1.1em solid #f50800;
  border-radius: 50%;
`;

const Requests = () => {
  const userDetails = JSON.parse(localStorage.getItem("user"));
  const {
    requests,
    setRequests,
    medicalRequests,
    setMedicalRequests,
    Acc,
    setAcc,
  } = useContext(RequestContext);
const[isError,setIsError]=useState(false)
const [openNoti, setOpenNoti] = useState(false);
const classes = useStyles();
const[isSent,setIsSent]=useState(false)
  requests.sort((a, b) => {
    return a.expenseFormId - b.expenseFormId;
  });

  medicalRequests.sort((a, b) => {
    return a.medicalFormId - b.medicalFormId;
  });

  const [openPopup, setOpenPopup] = useState(false);
  const [idtoupd, setIdtoupd] = useState(null);
  const [act, setAct] = useState(null);
  const [decision, setDecision] = useState(false);
  const [claimType, setClaimType] = useState("");
  const [remark, setRemark] = useState("");
  const [loading, setLoading] = useState(true);

  const changeStatus = (requestNo, func, type) => {
    setOpenPopup(true);
    setIdtoupd(requestNo);
    setAct(func);
    setClaimType(type);
  };

  useEffect(async () => {
    if (medicalRequests.length !== 0) {
      setLoading(false);
    }
    if (requests.length !== 0) {
      setLoading(false);
    }
    getMedicalClaims(userDetails.employee.employeeId)
      .then((res) => {
        setMedicalRequests(res);
        console.log(medicalRequests);

        setLoading(false);
      })
      .catch((err) => {
        setIsError(true)
          setOpenNoti(true);
        console.log(err);
        setLoading(false);
      });

    getExpenseClaims(userDetails.employee.employeeId)
      .then((res) => {
        setRequests(res);
        console.log(requests);

        setLoading(false);
      })
      .catch((err) => {
        setIsError(true)
          setOpenNoti(true);
        console.log(err);
        setLoading(false);
      });

    getAccoutant(userDetails.id)
      .then((res) => {
        setAcc(res);
      })
      .catch((err) => {
        setIsError(true)
          setOpenNoti(true);
        console.log(err);
      });
  }, []);

  useEffect(() => {
    if (decision && claimType === "expense") {
      let changedRequest = requests.find(
        (request) => request.expenseFormId === idtoupd
      );

      const obj = {
        receiverEmail: changedRequest.employee.email,
        subject: "",
        text: "",
      };

      const email = Acc.map((name) => {
        return name.email;
      });
      const newValue = email.find((email) => email != undefined);
      console.log(newValue);

      const objAccount = {
        receiverEmail: newValue,
        subject: "",
        text: "",
      };

      if (act === 1) {
        changedRequest.progressStatus = "accepted by tech lead";
        obj.subject = "Request accepted by techlead";

        obj.text = `The request of id ${changedRequest.expenseFormId} submitted on ${changedRequest.submittedDate} 
        has been accepted by techlead. the request is now being forwarded to accountant for further processing`;

        objAccount.subject = " There is a claim for review";

        objAccount.text = `Claim submitted on ${changedRequest.submittedDate} by ${changedRequest.employee.firstName} 
        has been accepted by techlead. Visit http://reimburserfrontend.s3-website.ap-south-1.amazonaws.com/ for futher details
        `;
      } else {
        changedRequest.progressStatus = "rejected by tech lead";
        changedRequest.remark = remark;
        obj.subject = "Request rejected by techlead";
        obj.text = `The request of id ${changedRequest.expenseFormId} submitted on ${changedRequest.submittedDate} 
        has been rejected by techlead due to ${remark}`;
      }

      updateExpenseClaim(changedRequest)
        .then((res) => {
          addEmail(obj)
            .then((res) => {})
            .catch((err) => {
              console.log(err);
            });

          if (act === 1) {
            addEmail(objAccount)
              .then((res) => {})
              .catch((err) => {
                console.log(err);
              });
          }

          const filteredItems = requests.filter(
            (request) => request.expenseFormId !== idtoupd
          );
          const newItems = [...filteredItems, res];
          setRequests(newItems);
          setIdtoupd(null);
          setAct(null);
          setDecision(false);
          setRemark("");
        })
        .catch((err) => {
          setIsSent(true)
            setOpenNoti(true);
          console.log(err);
        });
    } else if (decision && claimType === "medical") {
      let changedRequest = medicalRequests.find(
        (request) => request.medicalFormId === idtoupd
      );

      const obj = {
        receiverEmail: changedRequest.employee.email,
        subject: "",
        text: "",
      };

      const email = Acc.map((name) => {
        return name.email;
      });
      const newValue = email.find((email) => email != undefined);
      console.log(newValue);

      const objAccount = {
        receiverEmail: newValue,
        subject: "",
        text: "",
      };

      if (act === 1) {
        changedRequest.progressStatus = "accepted by tech lead";
        obj.subject = "Request accepted by techlead";
        obj.text = `The request of id ${changedRequest.medicalFormId} submitted on ${changedRequest.submittedDate} 
          has been accepted by techlead. the request is now being forwarded to accountant for further processing`;

        objAccount.subject = " There is a claim for review";
        objAccount.text = `Claim submitted on ${changedRequest.submittedDate} by ${changedRequest.employee.firstName} 
        has been accepted by techlead. Visit http://hexclanbucket1.s3-website.us-east-2.amazonaws.com/ for futher details
        `;
      } else {
        changedRequest.progressStatus = "rejected by tech lead";
        changedRequest.remark = remark;
        obj.subject = "Request rejected by techlead";
        obj.text = `The request of id ${changedRequest.medicalFormId} submitted on ${changedRequest.submittedDate} 
        has been rejected by techlead due to ${remark}`;
      }

      updateMedicalClaim(changedRequest)
        .then((res) => {
          addEmail(obj)
            .then((res) => {})
            .catch((err) => {
              console.log(err);
            });

          if (act === 1) {
            addEmail(objAccount)
              .then((res) => {})
              .catch((err) => {
                console.log(err);
              });
          }

          const filteredItems = medicalRequests.filter(
            (request) => request.medicalFormId !== idtoupd
          );
          const newItems = [...filteredItems, res];
          setMedicalRequests(newItems);
          setIdtoupd(null);
          setAct(null);
          setDecision(false);
          setRemark("");
        })
        .catch((err) => {
          setIsSent(true)
            setOpenNoti(true);
          console.log(err);
        });
    }
  }, [decision]);
const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (
    <Styles>
      <div>
        {loading && <ClipLoader css={override} loading={loading} size={150} />}
 {isError && <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        ><Alert severity="error" onClose={handleClose1}>Something Has Happened While Fetching Data. Try Again</Alert></Snackbar>}
{isSent && <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        ><Alert severity="error"  onClose={handleClose1}>Something Has Happened While Updating Data. Try Again</Alert></Snackbar>}
        <h3>Expense Claim</h3>
      </div>

      <ReqTable
        requests={requests}
        url="/techlead/requests"
        type="expense"
        changeStatus={changeStatus}
      />

      <div className="medical">
        <h3>Medical Claim</h3>
      </div>

      <ReqTable
        requests={medicalRequests}
        url="/techlead/requests"
        type="medical"
        changeStatus={changeStatus}
      />

      <Confirmpopup
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        act={act}
        setAct={setAct}
        decision={decision}
        setDecision={setDecision}
        remark={remark}
        setRemark={setRemark}
      ></Confirmpopup>
    </Styles>
  );
};

export default Requests;
