import { React, useState, useContext, useEffect } from "react";

import styled from "styled-components";
import { ContextReport } from "../../context/ContextReport";

import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import { Link } from "react-router-dom";
import Hidden from "@material-ui/core/Hidden";
import { getExpenseClaimsEmp } from "../../services/ExpenseForm";

export default function Expense() {
  
  const { ERepo, setERepo } = useContext(ContextReport);
const [isError,setIsError]=useState(false)
  const userDetails = JSON.parse(localStorage.getItem("user"));


  useEffect(async () => {
   

    getExpenseClaimsEmp(userDetails.employee.employeeId)
      .then((res) => {
        setERepo(res);
      })
      .catch((err) => {
        setIsError(true)
        console.log(err);
      });
  }, []);


  const expReports = ERepo.filter(
    (request) => request.progressStatus === "pending"
  ).map(({ expenseFormId: formId, ...rest }) => ({
    formId,
    reqtype: "expense",
    ...rest,
  }));

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  return (
    <ExpenseWrap>
      <div className="container table-responsive etable">
        <h4>Expense Progress </h4>
        <br />
        <table className="table table-hover">
          <thead style={{ backgroundColor: "#ec2329", color: "white" }}>
            <tr>
              <th scope="col">Form ID</th>
              <th scope="col">Report title</th>

              <Hidden smDown >
              <th scope="col">Submitted date</th>
              </Hidden>
              <th scope="col">Amount</th>
              <th scope="col">Status</th>
              <th scope="col"> Action</th>
            </tr>
          </thead>
          <tbody>
          
          
             
              
                  <>
                  
                     {expReports.map((expense) => {
                          return (
                      <tr  key={expense.formId}>
                        <th scope="row">{expense.formId}</th>
                        <td>{capitalizeFirstLetter(expense.formDescription)} </td>
                        <Hidden smDown >
                        <td>{expense.submittedDate}</td>
                        </Hidden>
                        <td>{expense.amount}</td>
                        <td>{capitalizeFirstLetter(expense.progressStatus)}</td>
                        <td>
                        <Link to={`/progress/${expense.formId}`}>
                  <LibraryBooksIcon color="secondary" />
                  </Link>
                        </td>
                        
                      </tr>
                       )  })}
                  </>
                
              
         
          </tbody>
        </table>
      </div>
    </ExpenseWrap>
  );
}

const ExpenseWrap = styled.div`
  margin-left: 0px;
  margin-top: 8px;

  @media (min-width: 992px) {
    margin-left: 250px;
  }
  thead {
    /* position: absolute; */
  }
  .etable {
    /* padding-left: 5rem; */
  }
`;
