import { React, useState, useContext, useEffect } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import { TablePagination, TableSortLabel, TextField } from "@material-ui/core";
import { SearchRounded } from "@material-ui/icons";
import InputAdornment from "@material-ui/core/InputAdornment";
import { Link } from "react-router-dom";
import { ContextReport } from "../../context/ContextReport";

import { getMedicalClaimsEmp } from "../../services/MedicalForm";
import { getExpenseClaimsEmp } from "../../services/ExpenseForm";
import Alert from '@material-ui/lab/Alert'




const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#EC2329",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(
  formId,
  reqtype,
  formDescription,
  submmitteddate,
  amount,
  appamount,
  status,
  
) {
  return { formId, reqtype, formDescription, submmitteddate, amount,appamount, status };
}

const useStyles = makeStyles({
  table: {
    minWidth: 700,
    width: "95%",
  },
});

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export default function Reports() {
  
  const { ERepo, setERepo, MRepo, setMRepo } = useContext(ContextReport);
const [isError,setIsError]=useState(false)
  const userDetails = JSON.parse(localStorage.getItem("user"));
  

  useEffect(async () => {
    getMedicalClaimsEmp(userDetails.employee.employeeId)
      .then((res) => {
        setMRepo(res);
      })
      .catch((err) => {
        setIsError(true)
        console.log(err);
      });

    getExpenseClaimsEmp(userDetails.employee.employeeId)
      .then((res) => {
        setERepo(res);
      })
      .catch((err) => {
        setIsError(true)
        console.log(err);
      });
  }, []);



  const medReports = MRepo.filter(
    (request) => request.progressStatus !== "pending"
  ).map(({ medicalFormId: formId, ...rest }) => ({
    formId,
    reqtype: "medical",
    ...rest,
  }));
  const expReports = ERepo.filter(
    (request) => request.progressStatus !== "pending"
  ).map(({ expenseFormId: formId, ...rest }) => ({
    formId,
    reqtype: "expense",
    ...rest,
  }));

  const mergedReports = expReports.concat(medReports);

  const rows = mergedReports.map((report) =>
    createData(
      report.formId,
      report.reqtype,
      report.formDescription,
      report.submittedDate,
      report.amount,
      report.approvedAmount,
      report.progressStatus
    )
  );


  const headCells = [
    { id: "formId", lable: "Form ID" },
    { id: "reqtype", lable: "Report type" },
    { id: "formDescription", lable: "Form description" },
    { id: "submmitteddate", lable: "Submitted date" },
    { id: "amount", lable: "Amount" },
    { id: "appamount", lable: "Approved amount" },
    { id: "status", lable: "Status" },
    { id: "action", lable: "Action" ,disableSorting:true  },
  ];

  const pages = [10,15,20];
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(pages[page]);
  const [order, setOrder] = useState();
  const [orderby, setOrderBy] = useState();
  const [filterFn, setFilterFn] = useState({
    fn: (items) => {
      return items;
    },
  });

  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  function getComparator(order, orderby) {
    return order === "desc"
      ? (a, b) => descendingComparator(a, b, orderby)
      : (a, b) => -descendingComparator(a, b, orderby);
  }

  function descendingComparator(a, b, orderby) {
    if (b[orderby] < a[orderby]) {
      return -1;
    }
    if (b[orderby] > a[orderby]) {
      return 1;
    }
    return 0;
  }

  const recordsAfterPagingAndSorting = () => {
    return stableSort(filterFn.fn(rows), getComparator(order, orderby)).slice(
      page * rowsPerPage,
      (page + 1) * rowsPerPage
    );
  };

  const handleSortRequest = (cellid) => {
    const isAsc = orderby === cellid && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(cellid);
  };

  const handleSearch = (e) => {
    let target = e.target;
    setFilterFn({
      fn: (items) => {
        if (target.value == "") return items;
        else
          return items.filter(
            (x) =>
              x.formDescription.toLowerCase().includes(target.value) ||
              x.submmitteddate.toLowerCase().includes(target.value) ||
              x.status.toLowerCase().includes(target.value)||
              x.reqtype.toLowerCase().includes(target.value)||
              x.formId.toString().includes(target.value)||
              x.amount.toString().includes(target.value)||
              x.appamount.toString().includes(target.value)
              
          );
      },
    });
  };

  return (
    <div>
      {isError && <Alert severity="error" style={{position:"absolute"}}>Something Has Happened While Fetching Data. Refresh The Page Again</Alert>}
      <TextField
        label="Search"
        onChange={handleSearch}
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchRounded />
            </InputAdornment>
          ),
        }}
        style={{ paddingBottom: "20px" }}
      />

      <TableContainer>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              {headCells.map((headcell) => (
                <StyledTableCell
                  align="left"
                  key={headcell.id}
                  sortDirection={orderby === headcell.id ? order : false}
                >
                  {headcell.disableSorting? headcell.lable:
                  <TableSortLabel
                    active={orderby === headcell.id}
                    direction={orderby === headcell.id ? order : "asc"}
                    onClick={() => {
                      handleSortRequest(headcell.id);
                    }}
                  >
                    {headcell.lable}
                  </TableSortLabel>}
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {recordsAfterPagingAndSorting().map((row) => (
              <StyledTableRow key={row.formId}>
                <StyledTableCell align="left">{row.formId}</StyledTableCell>
                <StyledTableCell align="left">  {capitalizeFirstLetter(row.reqtype)} </StyledTableCell>
                <StyledTableCell align="left">
                {capitalizeFirstLetter(row.formDescription)}
                </StyledTableCell>
                <StyledTableCell align="left">
                  {row.submmitteddate}
                </StyledTableCell>
                <StyledTableCell align="left">{row.amount}</StyledTableCell>
                <StyledTableCell align="left">{row.appamount}</StyledTableCell>
                <StyledTableCell align="left"> {capitalizeFirstLetter(row.status)}</StyledTableCell>
                <StyledTableCell align="center">
                  {/* <LibraryBooksIcon/> */}
                  <Link to={`/report/${row.formId}`}>
                    <LibraryBooksIcon color="secondary" />
                  </Link>
                </StyledTableCell>
              </StyledTableRow>

           
            ))}
          </TableBody>
        </Table>
        <TablePagination
          component="div"
          page={page}
          rowsPerPageOptions={pages}
          rowsPerPage={rowsPerPage}
          count={rows.length}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
           style={{ marginRight: "6rem" }}
        />
      </TableContainer>
    </div>
  );
}
