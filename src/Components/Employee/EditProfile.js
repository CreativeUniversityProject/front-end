import { Grid,TextField,Button} from "@material-ui/core";

import { React, useState, useContext, useEffect } from "react";
import { AppBar, Box, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";


import Popup from "./PopupE";
//import { ContextState } from "../../../context/contexStates";
import { AdminContext } from "../../context/adminContext";


import { useHistory,Link } from "react-router-dom";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";


import { editProfile } from "../../services/user";


const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiFormControl-root": {
      width: "80%",
      margin: theme.spacing(2),
      marginTop: "2rem",
    },
  },
  Button: {
    flex: "right",
    marginTop: "3rem",
    color: "white",
    marginBottom: "2rem",
  },
  newbutton: {
    marginLeft: "0rem",
    marginBottom: "1rem",
  },
  Paper1: {
    margin: theme.spacing(1),
    padding: theme.spacing(1),
  },
  paper: {
    background: "#194350",
    color: "#fff",
    boxshadow:
      " 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08)",
    padding: "0.1px 50px",
    width: "100%",
    marginTop: "0.1rem",
    
  },

  main:{
    marginLeft:"17rem",
    [theme.breakpoints.down('sm')]: {
      marginLeft:"0rem"
    },
  },

  Snackbar: {
    top: theme.spacing(9),
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function EditProfile() {
  const userDetails=JSON.parse(localStorage.getItem('user'))
const [isError, setIsError] = useState(false);
 const [userDetails1, setUserDetails1] = useState();
const initialFValues = {
  firstName:userDetails.employee.firstName,
  lastName:userDetails.employee.lastName,
  phoneNo:userDetails.employee.phoneNo,
  address:userDetails.employee.address
};
  const validate = (fieldValues = values) => {
    let temp = { ...errors };

    if ("firstName" in fieldValues)
      temp.firstName = fieldValues.firstName
        ? ""
        : "This field is required.";

    if ("lastName" in fieldValues)
      temp.lastName = fieldValues.lastName ? "" : "This field is required.";
if ("address" in fieldValues)
      temp.address = fieldValues.address ? "" : "This field is required.";

            if ('phoneNo' in fieldValues)
            temp.phoneNo = (/[0]{1}\d{9}/).test(fieldValues.phoneNo)&&fieldValues.phoneNo.length==10? "" : "Check the phone number again."
    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };

  const classes = useStyles();

  const [values, setValues] = useState(initialFValues);
  const [openPopup, setOpenPopup] = useState(false);

  const [errors, setErrors] = useState({});
  const [openNoti, setOpenNoti] = useState(false);
const[isSuccess,setIsSuccess]=useState(false);
  console.log(errors);



  const history = useHistory();



  

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      const object1 = {
        employeeId: userDetails.employee.employeeId,
        firstName: values.firstName,
        lastName: values.lastName,
        phoneNo: values.phoneNo,
        address: values.address,
        
      };

     
      

      editProfile(object1)
        .then((res) => {
          localStorage.setItem('user',JSON.stringify(res))
      setUserDetails1(res.employee)
      setIsSuccess(true);
        })
        .catch((err) => {
          setIsError(true)
          console.log(err);
        });
 
      resetForm();
    }
  };

 

  const handleClick = () => {
    if (validate()) {
      setOpenNoti(true);
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };

  const resetForm = () => {
     
     
    const initialFValues1 = {
     firstName: values.firstName,
        lastName: values.lastName,
        phoneNo: values.phoneNo,
        address: values.address,
};
    setValues(initialFValues1);
    setErrors({});
    setIsSuccess(false)
    setIsError(false)
   // history.push("/employee/edit/1013");
  };

  return (
    <div className={classes.main}>
      {isError &&  <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose}
        ><Alert onClose={handleClose} severity="error">Something Has Happened While Updating Data. Try Again</Alert></Snackbar>}
      
      <Paper className={classes.paper} elevation={3}>
            <h2 className="text-center" style={{  fontWeight: "bold" }}>Edit Profile</h2>
          </Paper>
          <form className={classes.root} onSubmit={handleSubmit}>
        <Grid container>
          <Grid item xs={12} sm={6}>
            <TextField
              variant="outlined"
              name="firstName"
              label="First Name"
              value={values.firstName}
              onChange={handleInputChange}
              error ={errors.firstName}
              color="secondary"
              helperText={errors.firstName}
              // error = {null}
              // {...(errors & { errors:true,helperText:errors})}

             
            />

            
          

        
            <TextField
              variant="outlined"
              name="lastName"
              label="Last Name"
              value={values.lastName}
              onChange={handleInputChange}
              color="secondary"
              helperText={errors.lastName}
              error={errors.lastName}
            />
<TextField
              variant="outlined"
              name="phoneNo"
              label="Phone Number"
              value={values.phoneNo}
              onChange={handleInputChange}
              error ={errors.phoneNo}
              color="secondary"
              helperText={errors.phoneNo}
             

             
            /><TextField
              variant="outlined"
              name="address"
              label="Address"
              value={values.address}
              onChange={handleInputChange}
              error ={errors.address}
              color="secondary"
              helperText={errors.address}
            

             
            />
         

            
      </Grid>
          </Grid>
       

        
        <Button
          type="submit"
          variant="contained"
          color="secondary"
          className={classes.Button}
          onClick={handleClick}
          style={{ marginLeft: '1.0rem' , fontWeight: "bold" }}
        >
          Submit
        </Button>

 {  isSuccess &&     <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose}
        >
          <Alert onClose={handleClose} severity="success">
            Form Submitted Succesfully
          </Alert>
        </Snackbar>}

        <Link to='/employee'>
        <Button
          type="reset"
          onClick={resetForm}
          variant="contained"
          color="primary"
          className={classes.Button}
          style={{ marginLeft: '5.0rem' , fontWeight: "bold" }}
        >
          Cancel
        </Button>
        </Link>
      </form>

      <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}></Popup>
    </div>
  );
}
