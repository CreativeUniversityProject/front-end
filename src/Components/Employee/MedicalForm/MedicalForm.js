import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Select,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Button,
  Paper,
} from "@material-ui/core";

import { React, useState, useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core";
import FormHelperText from '@material-ui/core/FormHelperText';
import MedicalTable from "./MedicalFormTable";
import AddIcon from "@material-ui/icons/Add";
import Popup from "./PopupM";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import { ContextState } from "../../../context/contexStates";
import {
  addFormM,
  getTech,
  getDependent,
  addEmail,
} from "../../../services/MedicalForm";

import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const initialFValues = {
  reporttitle: "",

  paymentmethode: "bank",
  doctorName: "",
  date: new Date(),
  techleadname: "",
  natureofinjury: "",
  personName: [],
  self: "self",
};

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiFormControl-root": {
      width: "80%",
      margin: theme.spacing(2),
    },
  },
  Button: {
    flex: "right",
    marginTop: "3rem",
    color: "white",
    marginBottom: "2rem",
  },
  newbutton: {
    marginLeft: "0rem",
    marginBottom: "1rem",
  },
  Paper1: {
    margin: theme.spacing(1),
    padding: theme.spacing(1),
  },
  Snackbar: {
    top: theme.spacing(9),
  },
}));

export default function MedicalForm() {
  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("reporttitle" in fieldValues)
      temp.reporttitle = fieldValues.reporttitle
        ? ""
        : "This field is required.";
    if ("doctorName" in fieldValues)
      temp.doctorName = fieldValues.doctorName ? "" : "This field is required.";
      if ("techleadname" in fieldValues)
      temp.techleadname = fieldValues.techleadname
        ? ""
        : "select one techlead.";
    if ("natureofinjury" in fieldValues)
      temp.natureofinjury = fieldValues.natureofinjury
        ? ""
        : "This field is required.";

    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };

  const classes = useStyles();

  const [values, setValues] = useState(initialFValues);
  const [openPopup, setOpenPopup] = useState(false);

  const [errors, setErrors] = useState({});

  const { records } = useContext(ContextState);
  const [records1, setRecords1] = records;
  const [openNoti, setOpenNoti] = useState(false);
  const history = useHistory();
  const [personName, setPersonName] = useState([]);

  const [techName, setTechName] = useState([]);
const[isError,setIsError]=useState(false)
const[isSuccess,setIsSuccess]=useState(false);

  useEffect(async () => {
    getTech(userDetails.id)
      .then((res) => {
        setTechName(res);
      })
      .catch((err) => {
        console.log(err);
      });

     

    getDependent(userDetails.employee.employeeId)
      .then((res) => {
        setPersonName(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const userDetails = JSON.parse(localStorage.getItem("user"));

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    setValues({
      ...values,
      [name]: value,
    });
     

  };

  
  console.log(values.personName)
  console.log(personName)


  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {

          const n = values.personName.length
          const obj =[]
          for(var i=0;i<n;i++){
             obj[i] = { dependentId: values.personName[i] }
            }
            

      const object1 = {
        receipt: records1,
        submittedDate: values.date,
        amount: TotalSum,
        self: dependent1,
        techleadId: values.techleadname,
        formDescription: values.reporttitle,
        doctorName: values.doctorName,
        diagnonsis: values.natureofinjury,
        progressStatus: "pending",
        paymentMethod: values.paymentmethode,
       
        employee: {
          employeeId: userDetails.employee.employeeId,
        },

        dependent:obj,
      };
      

      const object2 = {
        receiverEmail: newValue,

        subject: "You got a medical claim request",

        text: "visit http://hexclanbucket1.s3-website.us-east-2.amazonaws.com/  for a claim request",
      };

      addEmail(object2)
        .then((res) => {})
        .catch((err) => {
          console.log(err);
        });

      addFormM(object1)
        .then((res) => {
          setIsSuccess(true)
          history.push("/medical");
        })
        .catch((err) => {
          setIsError(true)
          console.log(err);
        });

      resetForm();
    }
  };

  const dependent1 =   values.self=="self" ? true : false
  

  const techEmail = techName.map((name) => {
    if (values.techleadname == name.employeeId) return name.email;
  });

  const newValue = techEmail.find((email) => email != undefined);

  const totalsum = records1.map((name) => {
    return parseInt(name.amount);
  });

  const TotalSum = totalsum.reduce((a, b) => a + b, 0);

  

  const resetForm = () => {
    setValues(initialFValues);
    setErrors({});
    setRecords1([]);
    setIsSuccess(false)
    setIsError(false)

  };

  const handleClick = () => {
    if (validate()) {
      setOpenNoti(true);
    }
  };

  const handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };

  return (
    <div>
     {isError &&  <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose}
        ><Alert onClose={handleClose} severity="error" >Something Has Happened While Submitting Form. Try Again</Alert></Snackbar>} 
         <form className={classes.root} onSubmit={handleSubmit}>
        <Grid container>
          <Grid item xs={12} sm={6}>
            <TextField
              variant="outlined"
              name="reporttitle"
              label="Report title"
              value={values.reporttitle}
              onChange={handleInputChange}
              error={errors.reporttitle}
              helperText={errors.reporttitle}
              color="secondary"
            />

            <FormControl>
              <FormLabel    color="secondary">Claim for</FormLabel>
              <RadioGroup
                name="self"
                value={values.self}
                onChange={handleInputChange}
                color="secondary"
              >
                <FormControlLabel
                  value="self"
                  control={<Radio />}
                  label="Self"
                />
                <FormControlLabel
                  value="dependent"
                  control={<Radio />}
                  label="Dependent"
                />
              </RadioGroup>
            {/* </FormControl>
            <FormControl> */}
              <Select
                multiple
                value={values.personName}
                onChange={handleInputChange}
                name="personName"
                disabled={values.self=="self"}
                color="secondary"
              >
                {personName.map((name) => (
                  <MenuItem key={name.dependentId} value={name.dependentId}>
                    {name.dependentName}
                  </MenuItem>
                ))}
              </Select>
              <FormHelperText>select if dependent</FormHelperText>
            </FormControl>

            <FormControl>
              <FormLabel    color="secondary">Payment method</FormLabel>
              <RadioGroup
                name="paymentmethode"
                value={values.paymentmethode}
                onChange={handleInputChange}
                color="secondary"
              >
                <FormControlLabel
                  value="bank"
                  control={<Radio />}
                  label="Bank"
                />
                <FormControlLabel
                  value="cash"
                  control={<Radio />}
                  label="Cash"
                />
              </RadioGroup>
            </FormControl>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              variant="outlined"
              name="doctorName"
              label="Doctor name"
              value={values.doctorName}
              onChange={handleInputChange}
              error={errors.doctorName}
              color="secondary"
              helperText={errors.doctorName}
            />

            <FormControl className={classes.formControl}>
              <InputLabel    color="secondary">Techlead name</InputLabel>
              <Select
                name="techleadname"
                value={values.techleadname}
                onChange={handleInputChange}
                error={errors.techleadname}
                helperText={errors.techleadname}
                color="secondary"
              >
                {techName.map((name) => {
                  return (
                    <MenuItem key={name.employeeId} value={name.employeeId}>
                      {name.firstName}
                    </MenuItem>
                  );
                })}
              </Select>
              <FormHelperText style={{color:"red"}} >{errors.techleadname}</FormHelperText>
            
            </FormControl>

            <TextField
              id="outlined-multiline-static"
              label="Diagnosis or nature of illness"
              multiline
              rows={3}
              variant="outlined"
              name="natureofinjury"
              error={errors.natureofinjury}
              helperText={errors.natureofinjury}
              value={values.natureofinjury}
              onChange={handleInputChange}
              color="secondary"
            />
            <TextField
              id="date"
              label="Date"
              type="date"
              defaultValue="2017-05-24"
              name="date"
              onChange={handleInputChange}
              color="secondary"
            />
          </Grid>
        </Grid>

        <Paper className={classes.Paper1}>
          <Button
            variant="outlined"
            startIcon={<AddIcon />}
            className={classes.newbutton}
            onClick={() => setOpenPopup(true)}
          >
            {" "}
            Add New{" "}
          </Button>

          <MedicalTable />
        </Paper>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className={classes.Button}
          onClick={handleClick}
        >
          Submit
        </Button>
      { isSuccess && <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose}
        >
          <Alert onClose={handleClose} severity="success">
            Form submitted succesfully
          </Alert>
        </Snackbar>}
        <Button
          type="reset"
          onClick={resetForm}
          variant="contained"
          color="secondary"
          className={classes.Button}
        >
          Reset
        </Button>
      </form>

      <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}></Popup>
    </div>
  );
}
