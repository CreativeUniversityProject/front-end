import Alert from "@material-ui/lab/Alert";
import React ,{ useContext,useState,useEffect } from "react";
import {Link} from 'react-router-dom'
import styled from "styled-components";
import {getProject } from "../../services/ExpenseForm";
import {EmpContext} from "../../context/ContextEmp"




import {addProfile, getProfileForEmployee, getProfiles,updateProfile} from '../../services/profilePicture'
import { getDependentForEmployee, getEmployeeBankAccount } from "../../services/user";
import Err from "../Error/Err";
import { css } from "@emotion/core";
import ClipLoader from "react-spinners/ClipLoader";

const override = css`
  position: absolute;
  top: calc(50% + 25em);
  left: calc(50% - 4em);
  width: 7em;
  height: 7em;
  border: 1.1em solid rgba(252, 28, 20, 0.2);
  border-left: 1.1em solid #f50800;
  border-radius: 50%;
 
`;

export default function EmployeeCard() {
         const [Emp,SetEmp]  = useContext(EmpContext)

 const [isError, setIsError] = useState(false);
         
const[image,setImage]=useState('')
const[isLoading,setIsLoading]=useState(false)
const[url,SetUrl]=useState()
const [projects, setProjects] = useState([]);
 const [loadingDependent, setLoadingDependent] = useState(true);
  const [loadingBank, setLoadingBank] = useState(true);
const [dependents, setDependents] = useState([]);
const userDetails=JSON.parse(localStorage.getItem('user'))
const [bank, setBank] = useState(null);

 useEffect(async () => {
    getProfileForEmployee(userDetails.employee.employeeId)
      .then((res) => {
        setIsLoading(true)
        setImage(res);
        
        console.log(image);
      })
      .catch((err) => {
 setIsError(true);

        console.log(err)
      });

      getProject(userDetails.employee.employeeId)
      .then((res) => {
        setProjects(res)
        console.log(res)
      })
      .catch((err) => {
       
        console.log(err);
      });
     getDependentForEmployee(userDetails.employee.employeeId)
      .then((res) => {
        setDependents(res);
        setLoadingDependent(false);
      })
      .catch((err) => {
     
        console.log(err);
        setLoadingDependent(false);
      });

    getEmployeeBankAccount(userDetails.employee.employeeId)
      .then((res) => {
        setBank(res);
        setLoadingBank(false);
      })
      .catch((err) => {
        
        console.log(err);
        setLoadingBank(false);
      });
  }, []);
  const uploadImage=async(e)=>{
    const files=e.target.files
    const data=new FormData()
    data.append('file',files[0])
    data.append('upload_preset','my_upload')
    const res=await fetch('	https://api.cloudinary.com/v1_1/hexclanuom/image/upload',{
      method:'POST',
      body:data
    })
    const file =await res.json()
    console.log(file)
    console.log(file.secure_url)
    SetUrl(file.secure_url)
    console.log(url)
  
   if(image[0]===null)
   {  const newProf={
     profilePicture:file.secure_url,
     employee:{
       employeeId:userDetails.employee.employeeId
     }
   }
   console.log(newProf)
     addProfile(newProf)
      .then((res) => {
     /*   getProfiles()
          .then((res) => {*/
            setImage(res);
            console.log(res)
          })
      /*    .catch((err) => {
            setIsError(true);
            console.log(err);
          });
      })*/
      .catch((err) => {
        setIsError(true);
        console.log(err);
      });
  }else{
const newProf={
  profilePicId:image.profilePicId,
     profilePicture:file.secure_url,
     employee:{
       employeeId:userDetails.employee.employeeId
     }
   }
   console.log(newProf)
     updateProfile(newProf)
      .then((res) => {
     /*   getProfiles()
          .then((res) => {*/
            setImage(res);
            console.log(res)
          })
        /*  .catch((err) => {
            setIsError(true);
            console.log(err);
          });
      })*/
      .catch((err) => {
        setIsError(true);
        console.log(err);
      });
  }
  }
  return (
    <>
     {isError && <Alert severity="error" style={{position:"center"}}>Something Has Gone With The Profile Picture. Refresh The Page Again</Alert>}
      <CardWrapper>
         
        <div className="container emp-profile" style={{  backgroundColor: "#e9ecef" }}>
          
            <div className="row">
              <div className="col-md-4">
                <div className="profile-img">
             {< img src={image.profilePicture} className="pro-img" alt="" style={{boxShadow:"0 4px 8px 0 rgba(0, 0, 0, 0.2),0 3px 10px 0 rgba(0, 0, 0, 0.19)"}} />}
                  <div className="file btn btn-lg btn-primary">
                    Change Photo
                    <input type="file" name="file" onChange={uploadImage} />
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="profile-head text-capitalize">
                  <h2 className="blackTextOverride" >{`${userDetails.employee.firstName} ${userDetails.employee.lastName}`}</h2>
                  <h6>{userDetails.employee.designation.designation}</h6>

                  <ul className="nav nav-tabs" id="myTab" role="tablist">
                    <li className="nav-item">
                      <div className="nav-link active" id="home-tab" style={{  backgroundColor: "#e9ecef" }}>
                        <h4 >About</h4>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-md-2 text-capitalize">
                <Link to={`/employee/edit/${userDetails.employee.employeeId}`}>
                <input
                  type="submit"
                  className="profile-edit-btn"
                  name="btnAddMore"
                  value="Edit Profile"
                  
                /></Link>
              </div>
            </div>
         

            <div className="row">
              <div className="col-md-4 text-capitalize">
                <div className="profile-work">

                  <h3 className="blackTextOverride">Projects</h3>
                 {projects.map((pro) => {
                        return (
                         
                           <div style={{marginLeft:"1rem",color:"darkslategrey"}}>{pro.projectName}</div>
                         
                        );
                      })}
            
                  

            
                </div>
                <br />
                
         
                <div className="col-4">
           
              <h3 className="blackTextOverride" >Dependents </h3>
          
          
               </div>
               
        
        <div className="ex1">

    
        
        <div className="back2 ">
        
          {dependents.map((dependent) => {
            return (
              <>
              <div>
                 <div className="pag">
                <div className="row">
                  <div className="col-md-6">
                  <label> Dependent ID:</label>
                  </div>
                  <div className="col-md-6 text-capitalize"><p>{dependent.dependentId}</p></div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <label> Name:</label>
                    </div>
                  <div className="col-md-6 text-capitalize"><p>{dependent.dependentName}</p></div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <label> Relationship:</label>
                    </div>
                  <div className="col-md-6 text-capitalize"><p>{dependent.relationship}</p></div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <label> Date of Birth:</label>
                  </div>
                  <div className="col-md-6 text-capitalize"><p>{dependent.dob}</p></div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <label> Gender:</label>
                  </div>
                  <div className="col-md-6 text-capitalize"><p>{dependent.gender}</p></div>
                </div>
                </div>
                </div>
                <br />
              </>
            );
          })}
               
              </div>
              </div>
              <br/><br/><br/><br/><br/><br/>
              </div>
             
         
            
              <div className="col-md-8" >
                <div className="tab-content profile-tab" id="myTabContent">
                  <div
                    className="tab-pane fade show active"
                    id="home"
                    role="tabpanel"
                    aria-labelledby="home-tab"
                  >
                    <div className="pag">
                    <div className="row">
                      <div className="col-md-6">
                        <label >Employee Id:</label>
                      </div>
                      <div className="col-md-6">
                        <p>{userDetails.employee.employeeId}</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6 ">
                        <label>Address:</label>
                      </div>
                      <div className="col-md-6 text-capitalize">
                        <p>{userDetails.employee.address}</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <label >Email:</label>
                      </div>
                      <div className="col-md-6">
                        <p>{userDetails.employee.email}</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <label>Phone:</label>
                      </div>
                      <div className="col-md-6">
                        <p>{userDetails.employee.phoneNo}</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <label>Department:</label>
                      </div>
                      <div className="col-md-6 text-capitalize">
                        <p>{userDetails.employee.department.department}</p>
                      </div>
                    </div>
                   <div className="row">
                      <div className="col-md-6 ">
                        <label>BirthDate:</label>
                      </div>
                      <div className="col-md-6 text-capitalize">
                        <p>{userDetails.employee.datetime}</p>
                      </div>
                    </div>
                       <div className="row">
                      <div className="col-md-6 ">
                        <label>NIC Number:</label>
                      </div>
                      <div className="col-md-6 text-capitalize">
                        <p>{userDetails.employee.nicNo}</p>
                      </div>
                    </div>
                    
                 </div>
        
        </div>
      
               
          
        
              
              <br/>
             
               <div className="col-6">
            
              <h3 className="blackTextOverride">Bank Details </h3>
           
          </div>
          
          <div className="pag">
          
              <div className="back2">
          

          {!loadingBank && (
            <>
              {bank === null ? (
                <>
                  <div className="container mt-5">
                  
                  </div>
                </>
              ) : (
                <>
                  
                  <div className="row">
                    <div className="col-md-6"> Bank:</div>
                    <div className="col-md-6 text-capitalize"><p>{bank.bankName}</p></div>
                  </div>
                  <div className="row">
                    <div className="col-md-6"> Branch:</div>
                    <div className="col-md-6 text-capitalize"><p>{bank.branch}</p></div>
                  </div>
                  <div className="row">
                    <div className="col-md-6"> Account Number:</div>
                    <div className="col-md-6 text-capitalize"><p>{bank.accountNo}</p></div>
                  </div>
                  
                </>
              )}
            </>
          )}
        </div>
        </div>
            
          </div>
          </div>
          
      </div>
          </div>

      </CardWrapper>
    </>
  );
}

const CardWrapper = styled.div`
  padding-left:0rem;
  margin-left: 0rem;

  @media (min-width: 992px) {
    margin-left: 13rem;
  }



  img {
    width: 100px;
    height: 100px;
  }
  body {
    background:#e9ecef; 
  }
  .emp-profile {
    padding: 3%;
    // margin-left:10%;
    margin-bottom: 3%;
    border-radius: 1.5rem;
    background: #fff;
  }
  .profile-img {
    text-align: center;
  }
  .profile-img img {
    width: 280px;
    height: 280px;
    
  }
  .profile-img .file {
    position: relative;
    overflow: hidden;
    margin-top: -20%;
    width: 70%;
    border: none;
    border-radius:1.5rem;
    font-size: 15px;
    background-color: #212529b8;
  }
  .profile-img .file input {
    position: absolute;
    opacity: 0;
    right: 0;
    top: 0;
    cursor: pointer;
  }
  .profile-head h5 {
    color: #333;
  }
  .profile-head h6 {
    color: #0062cc;
  }
  .profile-edit-btn {
    border: none;
    border-radius: 1.5rem;
    width: 70%;
    padding: 5%;
    font-weight: 600;
    color: #fff;
    cursor: pointer;
    background-color:#f71e1e;
    opacity:0.85;

  }
  .proile-rating {
    font-size: 12px;
    color: #818182;
    margin-top: 5%;
  }
  .proile-rating span {
    color: #495057;
    font-size: 15px;
    font-weight: 600;
  }
  .profile-head .nav-tabs {
    margin-bottom: 5%;
  }
  .profile-head .nav-tabs .nav-link {
    font-weight: 600;
    border: none;
  }
  .profile-head .nav-tabs .nav-link.active {
    border: none;
    border-bottom: 2px solid #0062cc;
  }
  .profile-work {
   margin-left:1rem;
  }

  .profile-work p {
    font-size: 12px;
    color: #818182;
    font-weight: 600;
   
  }
  // .profile-work a {
  //   text-decoration: none;
  //   color: #495057;
  //   font-weight: 600;
  //   font-size: 14px;
  // }
  .profile-work ul {
    list-style: none;
  }
  .profile-tab label {
    font-weight: 600;
  }
  .profile-tab p {
    font-weight: 600;
    color: #ffffff;
    padding: 1%;
  }
.pag{
  background-color: #000;
  opacity: 0.8;
  border-radius: 1.5rem;
  
  padding: 4%;

}
.row{
  color: #fff;

}
.blackTextOverride{
 color: black !important;
}

.col-md-6{
  font-weight: 600;
  


}
div.ex1 {
 
  height: 340px;
  width: 340px;
  overflow-y: scroll;
  overflow-x: hidden;

  @media (min-width: 992px) {
    height: 220px;
    width: 340px;
    overflow-y: scroll;
    overflow-x: hidden;
  }
}

.col-md-8{
  margin-left: 0rem;
  margin-top: -7rem;

  
  // @media (min-width: 992px) {
  //   margin-left: 23rem;
  //   margin-top: -30rem;
  // }
  
}





`;




