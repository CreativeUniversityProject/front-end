import {Grid,TextField,Button} from "@material-ui/core";

import { React, useState, useContext, useEffect } from "react";

import { makeStyles } from "@material-ui/core";
import { AppBar, Box, Paper } from "@material-ui/core";

import Popup from "./PopupE";
//import { ContextState } from "../../../context/contexStates";


import { useHistory } from "react-router-dom";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";


import { editProfile } from "../../services/user";
import { changePassword } from "../../services/employee";
import PasswordStrengthIndicator from "./PasswordStrengthIndicator";
const userDetails=JSON.parse(localStorage.getItem('user'))
const initialFValues = {
  oldPassword:'',
  newPassword:'',
  recheckPassword:'',
 
};

const useStyles = makeStyles((theme) => ({
  root: {
   marginLeft:"-1rem",
    backgroundColor:"#fff",
    padding:"1rem",
    width: "100%",
    boxShadow:"0 8px 16px 0 rgba(0, 0, 0, 0.2),0 6px 20px 0 rgba(0, 0, 0, 0.19)",

    "& .MuiFormControl-root": {
      width: "80%",
      margin: theme.spacing(2),
      marginTop: "2rem",
      
      
      
    },
  },
  container:{
    background: "#fff",
    
    
  },
  Button: {
    flex: "right",
    marginTop: "3rem",
    color: "white",
    marginBottom: "2rem",
    padding:"0.5rem"
  },
  newbutton: {
    marginLeft: "0rem",
    marginBottom: "1rem",
  },
  Paper1: {
    margin: theme.spacing(1),
    padding: theme.spacing(1),
  },
  Snackbar: {
    top: theme.spacing(9),
  },
  paper: {
    background: "#194350",
    color: "#fff",
    boxshadow:" 0 4px 6px rgba(50, 50, 93, 0.11), 0 1px 3px rgba(0, 0, 0, 0.08)",
    padding: "0.1px 50px",
    width: "100%",
    marginTop: "0.1rem",
    marginLeft:"-1rem",
    
  },
  main:{
    marginLeft:"17rem",
    [theme.breakpoints.down('sm')]: {
      marginLeft:"0rem",
      padding: "0.1px 50px",

    },
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const isNumberRegx = /\d/;
const specialCharacterRegx = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
const capitalCharRegx=/[A-Z]/;
export default function ChangePassword() {
 const [isError, setIsError] = useState(false); 
  const [password, setPassword] = useState("");
      const [passwordValidity, setPasswordValidity] = useState({
        minChar: null,
        number: null,
        specialChar: null,
        capitalChar:null
    });
  const validate = (fieldValues = values) => {
    let temp = { ...errors };

    if ("oldPassword" in fieldValues)
      temp.oldPassword = fieldValues.oldPassword
        ? ""
        : "This field is required.";

     if ("newPassword" in fieldValues)
      temp.newPassword = fieldValues.newPassword
        ? ""
        : "This field is required.";
    
            
if ("recheckPassword" in fieldValues)
      temp.recheckPassword = fieldValues.recheckPassword==fieldValues.newPassword ? "" : "Do not match. Recheck.";

    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };

  const classes = useStyles();
const[isSuccess,setIsSuccess]=useState(false);
  const [values, setValues] = useState(initialFValues);
  const [openPopup, setOpenPopup] = useState(false);

  const [errors, setErrors] = useState({});
  const [openNoti, setOpenNoti] = useState(false);

  // console.log(errors);
  // console.log(project)

  //const { ERecords, url } = useContext(ContextState);
 // const [ERecords1, setERecords1] = ERecords;

  const history = useHistory();

  

  const [techName, setTechName] = useState([]);

  

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    setValues({
      ...values,
      [name]: value,
    });
    if(name=='newPassword')
   {setPassword(value);

        setPasswordValidity({
            minChar: password.length >= 8 ? true : false,
            number: isNumberRegx.test(password) ? true : false,
            specialChar: specialCharacterRegx.test(password) ? true : false,
            capitalChar:capitalCharRegx.test(password)?true:false
        });}
        
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      const object1 = {
        userName:userDetails.email,
        oldPassword: values.oldPassword,
        newPassword: values.newPassword,
       
      
        
      };

     

      console.log(object1)

      changePassword(object1)
        .then((res) => {
       setIsSuccess(true)
          history.push("/employee");
        })
        .catch((err) => {
        
          console.log(err);
           setIsError(true);
        });

      resetForm();
    }
  };

 

  const handleClick = () => {
    console.log(isError);
    if (validate()) {
      setOpenNoti(true);
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };

  const resetForm = () => {
   
    setValues(initialFValues);
    setErrors({});
    setIsSuccess(false)
setIsError(false)
 
    
    //setERecords1([]);
   
  };
const goBack=()=>{
     history.push("/employee");
}
  return (
    <div className={classes.container}>
    <div  className={classes.main} >
     
      
      <Paper className={classes.paper} elevation={3}>
            <h2 className="text-center" style={{  fontWeight: "bold" }}>Change Password</h2>
          </Paper>
      <form className={classes.root} onSubmit={handleSubmit}>
        <Grid container>
          <Grid item xs={12} sm={6}>
            <TextField
              variant="outlined"
              name="oldPassword"
              label="Old Password"
                type="password"
              value={values.oldPassword}
              onChange={handleInputChange}
              error ={errors.oldPassword}
              color="secondary"
              
              helperText={errors.oldPassword}
              // error = {null}
              // {...(errors & { errors:true,helperText:errors})}

             
            />

            
          

        
            <TextField
              variant="outlined"
              name="newPassword"
              label="New Password"
                type="password"
              value={values.newPassword}
              onChange={handleInputChange }
              color="secondary"
              helperText={errors.newPassword}
              error={errors.newPassword}
            />
            <PasswordStrengthIndicator
                                        validity={passwordValidity}
                                    />
<TextField
              variant="outlined"
              name="recheckPassword"
              label="Recheck Password"
                type="password"
              value={values.recheckPassword}
              onChange={handleInputChange}
              error ={errors.recheckPassword}
              color="secondary"
              helperText={errors.recheckPassword}
              // error = {null}
              // {...(errors & { errors:true,helperText:errors})}

             
            />
            {/* 
           <TextField
                       variant="outlined"
                        name="techleadid"
                        label="Techlead name"
                        value={values.techleadid}
                        onChange={handleInputChange}
                        error={errors.techleadid}
                    /> */}

            
      </Grid>
          </Grid>
       

        
        <Button
          type="submit"
          variant="contained"
          color="secondary"
          className={classes.Button}
          onClick={handleClick}
          style={{ marginLeft: '1.0rem' , fontWeight: "bold" }}
        >
          Submit
        </Button>
 {isError &&(<Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose}
        > <Alert onClose={handleClose} severity="error">Check Your Old Password Again</Alert></Snackbar>)}
       {isSuccess && (<Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose}
        >
          <Alert onClose={handleClose} severity="success">
            Form Submitted 
          </Alert>
        </Snackbar>)
}
        <Button
          type="reset"
          onClick={goBack}
          variant="contained"
          color="primary"
          marginright="10rem"
          className={classes.Button}
          style={{ marginLeft: '5.0rem' , fontWeight: "bold" }}
        >
          Cancel
        </Button>
      </form>

      <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}></Popup>
    </div>
    </div>
  );
}

