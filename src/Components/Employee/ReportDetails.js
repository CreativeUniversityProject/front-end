import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { AccountantContext } from "../../context/accountantContext";
import { Grid, Hidden, makeStyles, Typography } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { ContextReport } from "../../context/ContextReport";
import { Paper } from "@material-ui/core";
import { getEmp } from "../../services/MedicalForm";
import Divider from "@material-ui/core/Divider";
import jsPDF from "jspdf";
import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/core";

const override = css`
  position: fixed;
  top: calc(60%);
  left: calc(50% - 4em);
  width: 9em;
  height: 9em;
  border: 1.1em solid rgba(252, 28, 20, 0.2);
  border-left: 1.1em solid #f50800;
  border-radius: 50%;
`;

const useStyles = makeStyles((theme) => ({
  main: {
    marginLeft: "16rem",
    marginTop: "5rem",
    borderRadius: "2.5rem",
    margin: theme.spacing(5),
    padding: theme.spacing(3),
    boxShadow:
      "0 8px 16px 0 rgba(0, 0, 0, 0.2),0 6px 20px 0 rgba(0, 0, 0, 0.19)",
    backgroundColor: "#fff",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "1rem",
    },
  },
  name: {
    fontSize: "2rem",
    margin: theme.spacing(3),
    padding: theme.spacing(2),
    fontWeight: "bold",
  },
  status: {
    fontSize: "1.2rem",
    borderRadius: "2.5rem",

    margin: theme.spacing(3),
    padding: theme.spacing(3),
  },
  text: {
    marginLeft: "2.5rem",
    // margin: theme.spacing(1),
    color: "white",
    fontSize: "1rem",
  },
  table: {
    marginLeft: "16rem",
    minWidth: 700,
    width: "78.4%",
  },
  reiceipt: {
    marginLeft: "16rem",
    margin: theme.spacing(5),
    backgroundColor: "#fff",
    paddingTop: "1rem",
    paddingLeft: "4rem",
    paddingRight: "10rem",
    marginTop: "1rem",
    borderRadius: "2.5rem",
    boxShadow:
      "0 8px 16px 0 rgba(0, 0, 0, 0.2),0 6px 20px 0 rgba(0, 0, 0, 0.19)",
    padding: "4rem",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "1rem",
      paddingLeft: "1rem",
    },
  },
  typo: {
    marginLeft: "17rem",
    fontSize: "2.5rem",
    fontWeight: "bold",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "2rem",
    },
  },
  img: {
    height: "20rem",
    width: "30rem",
    paddingLeft: "0rem",
    [theme.breakpoints.down("sm")]: {
      height: "18rem",
      width: "17rem",
      paddingTop: "1rem",
    },
  },
}));

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export default function ReportsDetails(props) {
  const classes = useStyles();
  const id = props.match.params.id;
  //   const { requests, medicalRequests } = useContext(AccountantContext);
  const { ERepo, MRepo, setERepo, setMRepo } = useContext(ContextReport);
  const [techName, setTechName] = useState([]);
  const [loading, setLoading] = useState(true);

  let type = "expense";
  let selectedRequest = {};
  selectedRequest = ERepo.find((request) => request.expenseFormId == id);
  if (typeof selectedRequest === "undefined") {
    type = "medical";
    selectedRequest = MRepo.find((request) => request.medicalFormId == id);
  }

  const { progressStatus, formDescription, submittedDate, amount } =
    selectedRequest;

  const { firstName, lastName } = selectedRequest.employee;

  let receipt = [];
  if (type === "medical") {
    receipt = selectedRequest.receipt;
  } else {
    receipt = selectedRequest.image;
  }

  let formId = "";
  if (type === "expense") {
    formId = selectedRequest.expenseFormId;
  } else {
    formId = selectedRequest.medicalFormId;
  }

  console.log(receipt);

  let color = "blue";

  if (progressStatus == "accepted by tech lead") {
    color = "#0dbd3c";
  } else if (progressStatus == "accepted by accountant") {
    color = "#0dbd3c";
  } else if (progressStatus == "rejected by accountant") {
    color = "red";
  } else if (progressStatus == "rejected by tech lead") {
    color = "red";
  }

  useEffect(async () => {
    getEmp(selectedRequest.techleadId)
      .then((res) => {
        setTechName(res);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }, []);

  const generatePDf = () => {
    var doc = new jsPDF("portrait", "px", "a4", "false");
    // doc.addImage();
    doc.setFont("Helvertica", "bold");
    // doc.setFillColor(225, 188, 183);
    // doc.getFillColor(225, 188, 183);
    doc.setLineWidth(2);
    doc.line(10, 10, 440, 10); //horizontal up ine
    doc.line(10, 620, 440, 620);
    doc.line(10, 10, 10, 620); // vertical left line
    doc.line(440, 10, 440, 620); //vertical right line
    doc.setFontSize(30);
    doc.setTextColor(205, 92, 92);
    //  doc.addImage(logo, 20, 40, 100, 100);
    doc.text(200, 40, "Reimbursement Report", "center");
    doc.setLineWidth(1);
    doc.setDrawColor(205, 92, 92);
    doc.line(80, 42, 320, 42);
    doc.setDrawColor(0, 0, 0);
    doc.setFontSize(15);
    doc.setTextColor(0, 0, 0);
    doc.text(50, 80, "Name: "); // X , Y, String
    doc.text(50, 100, "Description: ");
    doc.text(50, 120, "Submitted Date: ");
    doc.text(50, 140, "Amount: ");
    doc.text(50, 160, "Submitted Status:");
    doc.text(50, 180, "Techlead:");

    if (type === "expense") {
      doc.text(50, 200, "Project:");
      doc.setTextColor(100, 149, 237);
      doc.text(100, 200, selectedRequest.project);
    } else {
      doc.setTextColor(0, 0, 0);
      doc.text(50, 200, "Doctor Name:");
      doc.setTextColor(100, 149, 237);
      doc.text(120, 200, selectedRequest.doctorName);
    }

    //doc.setTextColor(131, 168, 233);
    doc.setTextColor(100, 149, 237);
    doc.text(100, 80, firstName + " " + lastName);
    doc.text(120, 100, formDescription);
    doc.text(140, 120, submittedDate);
    doc.text(100, 140, "" + amount + ".Rs");
    doc.text(140, 160, progressStatus);
    doc.text(120, 180, techName.firstName);

    doc.setFontSize(23);
    doc.setTextColor(205, 92, 92);
    doc.text(50, 280, "Receipts");
    doc.setLineWidth(1);
    doc.setDrawColor(205, 92, 92);
    doc.line(49, 282, 115, 282);
    doc.setDrawColor(0, 0, 0);
    doc.setFontSize(15);
    let counter = 0;
    receipt.map((rec) => {
      if (counter % 2 != 0) {
        doc.addPage();
        doc.setLineWidth(2);
        doc.line(10, 10, 440, 10); //horizontal up ine
        doc.line(10, 620, 440, 620);
        doc.line(10, 10, 10, 620); // vertical left line
        doc.line(440, 10, 440, 620); //vertical right line
        doc.setTextColor(0, 0, 0);
        doc.text(50, 60, "Description:");
        doc.text(50, 80, "Amount:");
        doc.text(50, 100, "Date: ");
        doc.text(50, 120, "Place: ");
        doc.text(50, 140, "Image:");

        doc.setTextColor(100, 149, 237);
        doc.text(120, 60, rec.description);
        doc.text(120, 80, "" + rec.amount + ".Rs");
        doc.text(120, 100, rec.date);
        doc.text(120, 120, rec.place);
        try {
          doc.addImage(rec.billImage, 120, 140, 200, 150);
        } catch (e) {
          doc.text(120, 140, "Image not available");
        }
      } else {
        doc.setTextColor(0, 0, 0);
        doc.text(50, 310, "Description:");
        doc.text(50, 330, "Amount:");
        doc.text(50, 350, "Date: ");
        doc.text(50, 370, "Place: ");
        doc.text(50, 390, "Image:");
        doc.setTextColor(100, 149, 237);
        doc.text(120, 310, rec.description);
        doc.text(120, 330, "" + rec.amount + ".Rs");
        doc.text(120, 350, rec.date);
        doc.text(120, 370, rec.place);
        try {
          doc.addImage(rec.billImage, 120, 390, 200, 150);
        } catch (e) {
          doc.text(120, 390, "Image not available");
        }
      }

      counter++;
    });

    doc.save(submittedDate + " Reimbursement");
  };

  return (
    <>
      {loading && <ClipLoader css={override} loading={loading} size={150} />}
      <Paper className={classes.main}>
        <Link to="/report">
          <button
            className="btn btn-danger"
            style={{ fontWeight: "bold", borderRadius: "1.5rem" }}
          >
            <ArrowBackIcon /> Go back
          </button>{" "}
        </Link>
        <button
          disabled={loading}
          className="btn btn-danger"
          style={{ fontWeight: "bold", borderRadius: "1.5rem" }}
          onClick={generatePDf}
        >
          Download Pdf
        </button>

        <Grid container spacing={0}>
          <Grid item xs>
            <div className={classes.name} style={{ fontWeight: "bold" }}>
              {firstName} {lastName}
              <div
                style={{
                  color: "black",
                  fontSize: "1rem",
                  fontWeight: "bold",
                  padding: 3,
                }}
              >
                Submitted on : {submittedDate}
              </div>
              <div
                style={{
                  color: "black",
                  fontSize: "1rem",
                  padding: 3,
                  fontWeight: "bold",
                }}
              >
                Description : {formDescription}
              </div>
              <div
                style={{
                  color: "black",
                  fontSize: "1rem",
                  padding: 3,
                  fontWeight: "bold",
                }}
              >
                Techlead : {techName.firstName}
              </div>
              {type === "expense" ? (
                <div
                  style={{
                    color: "black",
                    fontSize: "1rem",
                    padding: 3,
                    fontWeight: "bold",
                  }}
                >
                  Project : {selectedRequest.project}
                </div>
              ) : null}
              {type === "medical" ? (
                <div
                  style={{
                    color: "black",
                    fontSize: "1rem",
                    padding: 3,
                    fontWeight: "bold",
                  }}
                >
                  Doctor name : {selectedRequest.doctorName}
                </div>
              ) : null}
              {type === "medical" ? (
                <div
                  style={{
                    color: "black",
                    fontSize: "1rem",
                    padding: 3,
                    fontWeight: "bold",
                  }}
                >
                  Diagnonsis : {selectedRequest.diagnonsis}
                </div>
              ) : null}
            </div>
          </Grid>
          <Grid item xs>
            <Hidden smDown>
              <Paper
                className={classes.status}
                style={{
                  background: color,
                  color: "white",
                  margin: 30,
                  fontWeight: "bold",
                  position: "absolute",
                  boxShadow:
                    "0 4px 8px 0 rgba(0, 0, 0, 0.2),0 3px 10px 0 rgba(0, 0, 0, 0.19)",
                }}
              >
                {capitalizeFirstLetter(progressStatus)}
              </Paper>
            </Hidden>
            <Hidden smUp>
              <div
                className={classes.status}
                style={{
                  background: color,
                  color: "black",
                  fontSize: "1rem",
                  padding: 3,
                  fontWeight: "bold",
                }}
              >
                {capitalizeFirstLetter(progressStatus)}
              </div>
            </Hidden>
          </Grid>
          <Hidden smDown>
            <Divider orientation="vertical" flexItem />
          </Hidden>

          <Grid item xs className={classes.status}>
            <div
              style={{
                color: "black",
                fontSize: "1.2rem",
                padding: 3,
                fontWeight: "bold",
              }}
            >
              Amount to be reimbursed : <div>{amount}</div>
            </div>

            {selectedRequest.approvedAmount !== 0 ? (
              <div
                style={{
                  color: "black",
                  fontSize: "1.2rem",
                  padding: 3,
                  fontWeight: "bold",
                }}
              >
                Approved Amount : <div>{selectedRequest.approvedAmount}</div>
              </div>
            ) : null}

            {selectedRequest.remark !== null ? (
              <div
                style={{
                  color: "black",
                  fontSize: "1.2rem",
                  padding: 3,
                  fontWeight: "bold",
                }}
              >
                Reason to reject :{" "}
                <div> {capitalizeFirstLetter(selectedRequest.remark)}</div>
              </div>
            ) : null}
          </Grid>
        </Grid>
      </Paper>
      <Typography className={classes.typo}>Receipts</Typography>

      <Paper className={classes.reiceipt}>
        {type === "expense"
          ? receipt.map((rec) => {
              return (
                <div key={rec.imgId}>
                  <Grid container>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      style={{
                        fontSize: "1rem",
                        color: "black",
                        fontWeight: "bold",
                      }}
                    >
                      <div> Form ID : {rec.imgId} </div> <br />
                      <div>Amount : {rec.amount} </div> <br />
                      <div> Description : {rec.description} </div> <br />
                      <div> Date : {rec.date} </div> <br />
                      <div> Place: {rec.place} </div> <br />
                    </Grid>

                    <Grid
                      item
                      xs={12}
                      sm={6}
                      style={{
                        fontSize: "1rem",
                        color: "black",
                        fontWeight: "bold",
                      }}
                    >
                      <div> Bill image : </div>
                      <img
                        src={rec.billImage}
                        alt={rec.imgId}
                        className={classes.img}
                      />
                    </Grid>
                  </Grid>
                  <Divider style={{ color: "black", height: "2px" }} />
                  <br />
                </div>
              );
            })
          : receipt.map((rec) => {
              return (
                <div key={rec.receiptId}>
                  <Grid container>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      style={{
                        fontSize: "1rem",
                        padding: 4,
                        color: "black",
                        fontWeight: "bold",
                      }}
                    >
                      <div> Form ID : {rec.receiptId} </div> <br />
                      <div>Amount : {rec.amount} </div> <br />
                      <div> Description : {rec.description} </div> <br />
                      <div> Date : {rec.date} </div> <br />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <img
                        src={rec.receiptImage}
                        alt={rec.receiptId}
                        className={classes.img}
                      />
                    </Grid>
                  </Grid>
                  <Divider style={{ color: "black", height: "2px" }} />
                  <br />
                </div>
              );
            })}
      </Paper>
    </>
  );
}
