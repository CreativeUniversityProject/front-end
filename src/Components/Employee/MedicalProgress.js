import { React, useState, useContext, useEffect } from "react";

import styled from "styled-components";
import { ContextReport } from "../../context/ContextReport";
import LibraryBooksIcon from "@material-ui/icons/LibraryBooks";
import { Link } from "react-router-dom";
import Hidden from "@material-ui/core/Hidden";
import { getMedicalClaimsEmp } from "../../services/MedicalForm";
import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/core";



const override = css`
  position: absolute;
  top: calc(50% + 10em);
  left: calc(50% - 4em);
  width: 9em;
  height: 9em;
  border: 1.1em solid rgba(252, 28, 20, 0.2);
  border-left: 1.1em solid #f50800;
  border-radius: 50%;
`;


export default function Expense() {
  const {  MRepo, setMRepo } = useContext(ContextReport);
  const [isError,setIsError]=useState(false)
  const [loading, setLoading] = useState(true);

    const userDetails = JSON.parse(localStorage.getItem("user"));
    
  
    useEffect(async () => {

      if (MRepo.length !== 0) {
        setLoading(false);
      }

      getMedicalClaimsEmp(userDetails.employee.employeeId)
        .then((res) => {
          setMRepo(res);
          setLoading(false);
        })
        .catch((err) => {
          setIsError(true)
          console.log(err);
          setLoading(false);
        });
  
     
    }, []);
  

  const medReports = MRepo.filter(
    (request) => request.progressStatus === "pending"
  ).map(({ medicalFormId: formId, ...rest }) => ({
    formId,
    reqtype: "medical",
    ...rest,
  }));


  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  return (
    <ExpenseWrap>
       {loading && <ClipLoader css={override} loading={loading} size={150} />}
      <div className="container table-responsive etable">
        <h4>Medical Progress </h4>
        <br />
        <table className="table table-hover">
          <thead style={{ backgroundColor: "#ec2329", color: "white" }}>
            <tr>
              <th scope="col">Form ID</th>
              <th scope="col">Report title</th>
              <Hidden smDown >
              <th scope="col">Submitted date</th>
              </Hidden>
              <th scope="col">Amount</th>
              <th scope="col">Status</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
          
             
              
                  <>
                  
                     {medReports.map((medical) => {
                          return (
                      <tr key={medical.formId}>
                        <th scope="row">{medical.formId}</th>
                        <td>{capitalizeFirstLetter(medical.formDescription)}</td>
                        <Hidden smDown >
                        <td>{medical.submittedDate}</td>
                        </Hidden>
                        <td>{medical.amount}</td>
                        <td>{capitalizeFirstLetter(medical.progressStatus)}</td>
                        <td>
                        <Link to={`/progress/${medical.formId}`}>
                  <LibraryBooksIcon color="secondary" />
                  </Link>
                        </td>
                      </tr>
                       )  })}
                  </>
                
              
         
          </tbody>
        </table>
      </div>
    </ExpenseWrap>
  );
}

const ExpenseWrap = styled.div`
  margin-left: 0px;
  margin-top: 8px;

  @media (min-width: 992px) {
    margin-left: 250px;
  }
  thead {
    /* position: absolute; */
  }
  .etable {
    /* padding-left: 5rem; */
  }
`;
