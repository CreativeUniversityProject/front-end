import {createMuiTheme  } from '@material-ui/core/styles' ;

const heblue="#EC2329"
const tyblue="#9F9F9F"



export default createMuiTheme({


       palette:{
           common:{
               blue1:`${heblue}`,
               blue2: `${tyblue}`
           },

           primary:{
              main:`${heblue}`
           },
           secondary:{
               main:`${tyblue}`
           }
       },
       typography:{

        tab:{
            fontFamily:"Raleway",
            textTransform:"none",
            fontWeight: 700,
            fontSize:"1rem",
           },
        side:{
            fontFamily:"Goldman",
            textTransform:"none",
            fontWeight: 700,
            fontSize:"1rem",
           },
       }

})