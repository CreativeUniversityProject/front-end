import React from "react"
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import twitter from '../assets/twitter.svg'
import instagram from '../assets/instagram.svg'
import facebook from '../assets/facebook.svg'

const useStyles = makeStyles(theme => ({
    toolbarMargin: {
        ...theme.mixins.toolbar
      },
    footer :{
        
        background: "#ec2329",
        width:"100%",
        height: 18,
        zIndex:1302,
        position:'fixed',
        marginTop:"0rem",
        
        bottom:0,
        left:0
       
    },
    maincontainer:{
        position:"absolute",
    },
    link:{
        
        color:"white",
        fontFamily:"Arial",
        fontSize:"0.75rem",
        fontWeight:"bold",
        // marginLeft: "center",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
       
 

    },
    // icon :{
    //     height:"2rem",
    //     width:"2rem"
    // },
    // socialContainer:{
    //     position:"absolute",
    //     marginTop:"0em",
    //     right:"3em",
    //     [theme.breakpoints.down('xs')]: {
    //         right:"1rem",
    //       },
    // }
}))

export default function Header() {
    const classes = useStyles()

return <footer className={classes.footer}>
  
    <Grid container  justify="center" className={classes.maincontainer}>
        
     <Grid item className={classes.link}  > 
        Copyright &copy; Creative Softwares 2021. All rights
                    reserved.
        </Grid>
    </Grid>


  
    </footer>
}