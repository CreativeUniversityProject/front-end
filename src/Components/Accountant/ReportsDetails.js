import React, { useContext } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { AccountantContext } from "../../context/accountantContext";
import { makeStyles } from "@material-ui/core/styles";

export default function ReportsDetails(props) {
  const id = props.match.params.id;
  const { requests, medicalRequests } = useContext(AccountantContext);

  let type = "expense";
  let selectedRequest = {};
  selectedRequest = requests.find((request) => request.expenseFormId == id);
  if (typeof selectedRequest === "undefined") {
    type = "medical";
    selectedRequest = medicalRequests.find(
      (request) => request.medicalFormId == id
    );
  }
  const {
    formDescription,
    progressStatus,
    submittedDate,
    amount,
    approvedAmount,
    remark,
  } = selectedRequest;
  const { firstName, lastName } = selectedRequest.employee;

  let receipt = [];
  if (type === "medical") {
    receipt = selectedRequest.receipt;
  } else {
    receipt = selectedRequest.image;
  }

  let formId = "";
  if (type === "expense") {
    formId = selectedRequest.expenseFormId;
  } else {
    formId = selectedRequest.medicalFormId;
  }

  return (
    <Styles>
      <div className="container">
        <div className="col-2">
          <Link to="/accountant/reports">
            <button className="btn btn-danger">Back</button>
          </Link>
        </div>

        <div className="col-6">
          <h3>Details</h3>
        </div>

        <div className="content">
          <div className="back">
            <div className="row">
              <div className="col-md-6"> Employee : </div>
              <div className="col-md-6">
                {firstName} {lastName}
              </div>
            </div>

            <div className="row">
              <div className="col-md-6"> Request No : </div>
              <div className="col-md-6">{formId}</div>
            </div>

            <div className="row">
              <div className="col-md-6"> Expense Type : </div>
              <div className="col-md-6">{formDescription}</div>
            </div>
            <div className="row">
              <div className="col-md-6"> Progress Status : </div>
              <div className="col-md-6">{progressStatus}</div>
            </div>
            {approvedAmount !== 0 ? (
              <div className="row">
                <div className="col-md-6"> Approved Amount </div>
                <div className="col-md-6">{approvedAmount}</div>
              </div>
            ) : null}
            {remark !== null ? (
              <div className="row">
                <div className="col-md-6"> Remark </div>
                <div className="col-md-6">{remark}</div>
              </div>
            ) : null}

            <div className="row">
              <div className="col-md-6"> Submitted Date : </div>
              <div className="col-md-6">{submittedDate}</div>
            </div>
            <div className="row">
              <div className="col-md-6"> Amount : </div>
              <div className="col-md-6">{amount}</div>
            </div>
          </div>
          <div></div>

          <div >
            <div className="col-7">
              <h3>Receipts</h3>
            </div>
            <br />
            <br />
            {/* {receipt.map(rec => {
            return(
              <div key={rec.receiptId}>
                {rec.receiptId}<br/>
                {rec.amount}<br/>
                {rec.description}<br/>
                <img src={rec.receiptImage} alt={rec.receiptId}/><br/>
                </div>
            )
          })} */}
            <div className="back2">
              {type === "expense"
                ? receipt.map((rec) => {
                    return (
                      <div key={rec.imgId}>
                        <div className="row">
                          <div className="col-md-6"> Image ID : </div>
                          <div className="col-md-6">
                            {rec.imgId}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Amount : </div>
                          <div className="col-md-6">
                            {rec.amount}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Description : </div>
                          <div className="col-md-6">
                            {rec.description}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6">Receipt Image :</div>
                          <img src={rec.billImage} alt={rec.imgId}   width={250}
                              height={250} />
                          <br />
                        </div>
                      </div>
                    );
                  })
                : receipt.map((rec) => {
                    return (
                      <div key={rec.receiptId}>
                        <div className="row">
                          <div className="col-md-6"> Receipt ID : </div>
                          <div className="col-md-6">
                            {rec.receiptId}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Amount : </div>
                          <div className="col-md-6">
                            {rec.amount}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Description : </div>
                          <div className="col-md-6">
                            {rec.description}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6">Receipt Image : </div>
                          
                          <div className="im">
                            <img
                              src={rec.receiptImage}
                              alt={rec.receiptId}
                              width={250}
                              height={250}
                            />
                            <br />
                          </div>
                        </div>
                      </div>
                    );
                  })}
            </div>
          </div>
        </div>
      </div>
    </Styles>
  );
}

const Styles = styled.div`
  padding-top: 3rem;
  margin-left: 0.4rem;

  @media (min-width: 992px) {
    margin-left: 20rem;
  }

  .content {
    margin-top: 3rem;
    margin-left: 1rem;

    @media (min-width: 992px) {
      margin-top: 3rem;
      margin-left: 3rem;
  }
  }

 

  .btn-danger {
  
    padding-right: 3rem;
    width:100%;
     
       font-weight: 500;
       background-color: #ec2329;
       cursor: pointer;
 
 
     @media (min-width: 992px) {
       margin-left: -3rem;
       box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
         0 6px 20px 0 rgba(0, 0, 0, 0.19);
         border-radius: 50px 25px;
       width: 70%;
       padding: 5%;
       font-weight: 900;
       background-color: #ec2329;
       cursor: pointer;
   }
   }

   .col-6 {
    
   
    border-radius: 50px 50px;
    width: 70%;
    padding: 3%;
    margin-left:4rem;
    font-weight:500;
    color: #fff;
    text-align: center;
    background-color: #ec2329;
    font-weight: bold;

    @media (min-width: 992px) {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 50px 50px;
    width: 50%;
    margin-left:12rem;
    padding: 1%;
    font-weight: 900;
    color: #fff;
    text-align: center;
    // background-color: #3b5998;
    background-color: #ec2329;
    font-weight: bold;
  }
  }
  .col-md-6{
    font-weight: 600;
  }


  .col-7 {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
    0 6px 20px 0 rgba(0, 0, 0, 0.19);
  border-radius: 50px 50px;
  width: 70%;
  padding: 3%;
  margin-left:4rem;
  font-weight: 500;
  color: #fff;
  text-align: center;
  // background-color: #3b5998;
  background-color: #ec2329;
  font-weight: bold;

  @media (min-width: 992px) {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
    0 6px 20px 0 rgba(0, 0, 0, 0.19);
  border-radius: 50px 50px;
  width: 50%;
  padding: 1%;
  font-weight: 900;
  color: #fff;
  text-align: center;
  // background-color: #3b5998;
  background-color: #ec2329;
  font-weight: bold;
  margin-left:10rem
}
  }

  .col-md-6 {
    font-weight: bold;
    color: #000;
  }
  .col-md-6 {
    font-weight: bold;
    font-weight: 900;
    color: #000;
    margin-bottom: 1rem;
  }

  .im {
    padding: 1%;
    margin-bottom: 3%;
    border-radius: 8px;
  }

  .back {

    background-color: #fff;
 
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 1.5rem;
     padding:5%;
    margin-bottom: 5%;
     
    
    @media (min-width: 992px) {
      background-color: #fff;
      color: #000;
      opacity: 0.8;
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      border-radius: 1.5rem;
      width: 80%;
      height: 100%;
      padding: 3%;
      margin-bottom: 3%;
  }
 }
 .back2 {
  background-color: #fff;

  box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
    0 6px 20px 0 rgba(0, 0, 0, 0.19);
  border-radius: 1.5rem;
   padding:5%;
  margin-bottom: 5%;
 
  @media (min-width: 992px) {
    background-color: #fff;
    color: #000;
    opacity: 0.8;
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 1.5rem;
    width: 80%;
    height: 100%;
    padding: 3%;
    margin-bottom: 3%;
}
}
`;
