import React, { useContext, useState, useEffect } from "react";
import { AccountantContext } from "../../context/accountantContext";
import styled from "styled-components";
import { Link, useHistory } from "react-router-dom";
import { updateExpenseClaim } from "../../services/ExpenseForm";
import { updateMedicalClaim } from "../../services/MedicalForm";
import Confirmpopup from "./Confirmpopup";
import { addEmail } from "../../services/ExpenseForm";
import { getEmployeeBankAccount } from "../../services/user";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import { makeStyles } from "@material-ui/core/styles";
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles((theme) => ({
 Snackbar: {
    top: theme.spacing(9),
  },
}));
export default function RequestDetails(props) {
  const id = props.match.params.id;
  let urlPart = props.match.path.replace(":id", "");
  const history = useHistory();
  const {
    requests,
    setRequests,
    medicalRequests,
    setMedicalRequests,
  } = useContext(AccountantContext);
const[isError,setIsError]=useState(false)
 const [openNoti, setOpenNoti] = useState(false);
  let type = "expense";
  let selectedRequest = {};
  selectedRequest = requests.find((request) => request.expenseFormId == id);
  if (typeof selectedRequest === "undefined") {
    type = "medical";
    selectedRequest = medicalRequests.find(
      (request) => request.medicalFormId == id
    );
  }

  console.log(selectedRequest);
  const {
    formDescription,
    progressStatus,
    submittedDate,
    amount,
    paymentMethod,
  } = selectedRequest;
  const { firstName, lastName, employeeId } = selectedRequest.employee;

  let receipt = [];
  if (type === "medical") {
    receipt = selectedRequest.receipt;
  } else {
    receipt = selectedRequest.image;
  }

  let formId = "";
  if (type === "expense") {
    formId = selectedRequest.expenseFormId;
  } else {
    formId = selectedRequest.medicalFormId;
  }
  const classes = useStyles();
  const [openPopup, setOpenPopup] = useState(false);
  const [act, setAct] = useState(null);
  const [decision, setDecision] = useState(false);
  const [approvedAmount, setApprovedAmount] = useState(0);
  const [remark, setRemark] = useState("");
  const [employeeBankAccount, setEmployeeBankAccount] = useState(null);
const[isSent,setIsSent]=useState(false)
  const changeStatus = (func) => {
    setOpenPopup(true);
    setAct(func);
  };

  const obj = {
    receiverEmail: selectedRequest.employee.email,
    subject: "",
    text: "",
  };

  useEffect(async () => {
    console.log(selectedRequest.employee.employeeId);
    getEmployeeBankAccount(selectedRequest.employee.employeeId)
      .then((res) => {
        setEmployeeBankAccount(res);
        console.log(res);
      })
      .catch((err) => {
        setOpenNoti(true);
        setIsError(true)
        console.log(err);
      });
  }, []);

  useEffect(() => {
    if (decision) {
      let changedRequest = selectedRequest;

      if (act === 1) {
        changedRequest.progressStatus = "accepted by accountant";
        changedRequest.approvedAmount = approvedAmount;
        obj.subject = "Request accepted by accountant";
        obj.text = `payment now being processed for ${approvedAmount}`;
      } else {
        changedRequest.progressStatus = "rejected by accountant";
        changedRequest.remark = remark;
        obj.subject = "Request rejected by accountant";
        obj.text = `request rejected due to ${remark}`;
      }

      if (type === "medical") {
        updateMedicalClaim(changedRequest)
          .then((res) => {
            addEmail(obj)
              .then((res) => {})
              .catch((err) => {
                
                console.log(err);
              });

            const filteredItems = medicalRequests.filter(
              (request) => request.medicalFormId !== formId
            );
            const newItems = [...filteredItems, res];
            setMedicalRequests(newItems);
            setAct(null);
            setDecision(false);
            setRemark("");
            setApprovedAmount(0);
            history.push("/accountant/requests");
          })
          .catch((err) => {
            setIsSent(true)
            setOpenNoti(true);
            console.log(err);
          });
      } else {
        updateExpenseClaim(changedRequest)
          .then((res) => {
            addEmail(obj)
              .then((res) => {})
              .catch((err) => {
               
                console.log(err);
              });

            const filteredItems = requests.filter(
              (request) => request.expenseFormId !== formId
            );
            const newItems = [...filteredItems, res];
            setRequests(newItems);
            setAct(null);
            setDecision(false);
            setRemark("");
            setApprovedAmount(0);
            history.push("/accountant/requests");
          })
          .catch((err) => {
         setIsSent(true)
         setOpenNoti(true);
            console.log(err);
          });
      }
    }
  }, [decision]);
const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (
    <Styles>
      <div className="container">
        {isError && <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        ><Alert severity="error"  onClose={handleClose1}>Something Has Happened While Fetching Data. Try Again</Alert></Snackbar>}
        {isSent && <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        ><Alert severity="error"  onClose={handleClose1}>Something Has Happened While Updating Data. Try Again</Alert></Snackbar>}
        <div className="row">
          <div className="col-2">
            <Link to="/accountant/requests">
              <button className="btn btn-danger">Go back</button>{" "}
            </Link>
          </div>
          <div className="col-6">
            <h3>Details</h3>
          </div>
          {props.match.path == "/accountant/requests/:id" ? (
            <div className="col-4">
              <i
                className="fas fa-check text-success"
                onClick={() => changeStatus(1)}
              ></i>
              <i
                className="fas fa-times text-danger"
                onClick={() => changeStatus(-1)}
              ></i>
            </div>
          ) : (
            <div className="col-4"></div>
          )}
        </div>

        <div className="content">
          <div className="back">
            <div className="row">
              <div className="col-md-6"> Employee: </div>
              <div className="col-md-6">
                {firstName} {lastName}
              </div>
            </div>

            <div className="row">
              <div className="col-md-6"> Request No: </div>
              <div className="col-md-6">{formId}</div>
            </div>

            <div className="row">
              <div className="col-md-6"> Expense Type: </div>
              <div className="col-md-6">{formDescription}</div>
            </div>
            <div className="row">
              <div className="col-md-6"> progress Status: </div>
              <div className="col-md-6">{progressStatus}</div>
            </div>
            <div className="row">
              <div className="col-md-6"> submitted Date: </div>
              <div className="col-md-6">{submittedDate}</div>
            </div>
            <div className="row">
              <div className="col-md-6"> Amount: </div>
              <div className="col-md-6">{amount}</div>
            </div>
            <div className="row">
              <div className="col-md-6"> Payment method: </div>
              <div className="col-md-6">{paymentMethod}</div>
            </div>
            {paymentMethod === "bank" ? (
              <div className="row">
                <div className="col-md-6"> Bank: </div>
                {employeeBankAccount !== null ? (
                  <div className="col-md-6">
                    {employeeBankAccount.accountNo} ,
                    {employeeBankAccount.bankName} ,{" "}
                    {employeeBankAccount.branch}
                  </div>
                ) : (
                  <div className="col-md-6">None available</div>
                )}
              </div>
            ) : null}
          </div>
          <div >
            <div className="col-7">
              <h3>Receipts</h3>
            </div>
            <br />
            <br />
            {/* {receipt.map(rec => {
            return(
              <div key={rec.receiptId}>
                {rec.receiptId}<br/>
                {rec.amount}<br/>
                {rec.description}<br/>
                <img src={rec.receiptImage} alt={rec.receiptId}/><br/>
                </div>
            )
          })} */}
            <div className="back2">
              {type === "expense"
                ? receipt.map((rec) => {
                    return (
                      <div key={rec.imgId}>
                        <div className="row">
                          <div className="col-md-6"> Image ID : </div>
                          <div className="col-md-6">
                            {rec.imgId}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Amount : </div>
                          <div className="col-md-6">
                            {rec.amount}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Description : </div>
                          <div className="col-md-6">
                            {rec.description}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6">Receipt Image :</div>
                          <img src={rec.billImage} alt={rec.imgId}  width={250}
                              height={250}/>
                          <br />
                        </div>
                      </div>
                    );
                  })
                : receipt.map((rec) => {
                    return (
                      <div key={rec.receiptId}>
                        <div className="row">
                          <div className="col-md-6"> Receipt ID : </div>
                          <div className="col-md-6">
                            {rec.receiptId}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Amount : </div>
                          <div className="col-md-6">
                            {rec.amount}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6"> Description : </div>
                          <div className="col-md-6">
                            {rec.description}
                            <br />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6">Receipt Image : </div>
                          <div className="im">
                          <img
                              src={rec.receiptImage}
                              alt={rec.receiptId}
                              width={250}
                              height={250}
                            />
                            <br />
                          </div>
                        </div>
                      </div>
                    );
                  })}
            </div>
          </div>
          <Confirmpopup
            openPopup={openPopup}
            setOpenPopup={setOpenPopup}
            act={act}
            setAct={setAct}
            decision={decision}
            setDecision={setDecision}
            approvedAmount={approvedAmount}
            setApprovedAmount={setApprovedAmount}
            remark={remark}
            setRemark={setRemark}
          ></Confirmpopup>
        </div>
      </div>
    </Styles>
  );
}

const Styles = styled.div`
padding-top: 3rem;
margin-left: 0.4rem;

i {
  font-size: 2rem;
  margin-left: 3rem;
}

@media (min-width: 992px) {
  margin-left: 250px;
}

.content {
  margin-top: 3rem;
  margin-left: 1rem;

  @media (min-width: 992px) {
    margin-top: 3rem;
    margin-left: 3rem;
  }
}

.col-md-6 {
  font-weight: 600;
}

  

  .btn-danger {
    padding-right: 3rem;
    width: 100%;

    font-weight: 500;
    background-color: #ec2329;
    cursor: pointer;

    @media (min-width: 992px) {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      border-radius: 25px 50px;
      width: 50%;
      padding: 5%;
      font-weight: 900;
      background-color: #ec2329;
      cursor: pointer;
    }
  }


  .col-6 {
    border-radius: 50px 50px;
    width: 50%;
    padding: 3%;
    margin-left: 2rem;
    font-weight: 500;
    color: #fff;
    text-align: center;
    background-color: #ec2329;

    @media (min-width: 992px) {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      margin-left: auto;
      margin-right: auto;
      border-radius: 50px 50px;
      width: 50%;
      padding: 1%;
      font-weight: 900;
      color: #fff;
      text-align: center;
      // background-color: #3b5998;
      background-color: #ec2329;
      font-weight: bold;
    }
  }

  .col-7 {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 50px 50px;
    width: 70%;
    padding: 3%;
    margin-left: 4rem;
    font-weight: 500;
    color: #fff;
    text-align: center;
    // background-color: #3b5998;
    background-color: #ec2329;
    font-weight: bold;

    @media (min-width: 992px) {
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      margin-left: 7rem;
      margin-right: auto;
      border-radius: 1.5rem;
      width: 50%;
      padding: 1%;
      font-weight: 900;
      color: #fff;
      text-align: center;
      // background-color: #3b5998;
      background-color: #ec2329;

      font-weight: bold;
    }
  }

  .col-md-6 {
    font-weight: bold;
    color: #000;
  }
  .col-md-6 {
    font-weight: bold;
    font-weight: 900;
    color: #000;
    margin-bottom: 1rem;
  }
  .im {
    padding: 1%;
    margin-bottom: 3%;
    border-radius: 8px;
  }



  .back {
    background-color: #fff;

    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 1.5rem;
    padding: 5%;
    margin-bottom: 5%;

    @media (min-width: 992px) {
      background-color: #fff;
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      color: #000;
      opacity: 0.8;
      border-radius: 1.5rem;
      width: 80%;
      height: 100%;
      padding: 3%;
      margin-bottom: 3%;
      margin-right: auto;
    }
  }
  .back2 {
    background-color: #fff;

    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    border-radius: 1.5rem;
    padding: 5%;
    margin-bottom: 5%;

    @media (min-width: 992px) {
      background-color: #fff;
      box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
        0 6px 20px 0 rgba(0, 0, 0, 0.19);
      opacity: 0.8;
      border-radius: 1.5rem;
      width: 80%;
      height: 100%;
      padding: 3%;
      margin-bottom: 3%;
    }
  }
`;
