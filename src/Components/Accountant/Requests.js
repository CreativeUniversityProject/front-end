import React, { useContext, useState, useEffect } from "react";
import { AccountantContext } from "../../context/accountantContext";
import styled from "styled-components";
import {
  updateExpenseClaim,
  getAllExpenseClaims,
} from "../../services/ExpenseForm";
import {
  updateMedicalClaim,
  getAllMedicalClaims,
} from "../../services/MedicalForm";
import Confirmpopup from "./Confirmpopup";
import ReqTable from "./ReqTable";
import { addEmail } from "../../services/ExpenseForm";
import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/core";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import { makeStyles } from "@material-ui/core/styles";
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles((theme) => ({
 Snackbar: {
    top: theme.spacing(9),
  },
}));

const Styles = styled.div`
  padding-top: 1rem;
  margin-left: 0.4rem;

  i {
    margin: 2.5px;
  }

  @media (min-width: 992px) {
    margin-left: 250px;

    i {
      margin: 2px;
    }
  }

  > .medical {
    margin-top: 3rem;
  }
`;

const override = css`
  position: absolute;
  top: calc(50% + 10em);
  left: calc(50% - 4em);
  width: 9em;
  height: 9em;
  border: 1.1em solid rgba(252, 28, 20, 0.2);
  border-left: 1.1em solid #f50800;
  border-radius: 50%;
`;

const Requests = () => {
  const {
    requests,
    setRequests,
    medicalRequests,
    setMedicalRequests,
  } = useContext(AccountantContext);
const[isError,setIsError]=useState(false)
const[isSent,setIsSent]=useState(false)
const [openNoti, setOpenNoti] = useState(false);
  useEffect(async () => {
    if (medicalRequests.length !== 0) {
      setLoading(false);
    }
    if (requests.length !== 0) {
      setLoading(false);
    }
    getAllMedicalClaims()
      .then((res) => {
        setMedicalRequests(res);
        setLoading(false);
      })
      .catch((err) => {
        setIsError(true)
        setOpenNoti(true);
        console.log(err);
        setLoading(false);
      });

    getAllExpenseClaims()
      .then((res) => {
        setRequests(res);
        setLoading(false);
      })
      .catch((err) => {
        setIsError(true)
        setOpenNoti(true);
        console.log(err);
        setLoading(false);
      });
  }, []);

  requests.sort((a, b) => {
    return a.expenseFormId - b.expenseFormId;
  });

  medicalRequests.sort((a, b) => {
    return a.medicalFormId - b.medicalFormId;
  });

  const [openPopup, setOpenPopup] = useState(false);
  const [idtoupd, setIdtoupd] = useState(null);
  const [act, setAct] = useState(null);
  const [decision, setDecision] = useState(false);
  const [claimType, setClaimType] = useState("");
  const [approvedAmount, setApprovedAmount] = useState(0);
  const [remark, setRemark] = useState("");
  const [loading, setLoading] = useState(true);
const classes = useStyles();
  const changeStatus = (requestNo, func, type) => {
    setOpenPopup(true);
    setIdtoupd(requestNo);
    setAct(func);
    setClaimType(type);
  };

  useEffect(() => {
    if (decision && claimType === "expense") {
      let changedRequest = requests.find(
        (request) => request.expenseFormId === idtoupd
      );

      const obj = {
        receiverEmail: changedRequest.employee.email,
        subject: "",
        text: "",
      };

      if (act === 1) {
        changedRequest.progressStatus = "accepted by accountant";
        changedRequest.approvedAmount = approvedAmount;
        obj.subject = "Request accepted by accountant";
        obj.text = `payment now being processed for ${approvedAmount}`;
      } else {
        changedRequest.progressStatus = "rejected by accountant";
        changedRequest.remark = remark;
        obj.subject = "Request rejected by accountant";
        obj.text = `request rejected due to ${remark}`;
      }

      updateExpenseClaim(changedRequest)
        .then((res) => {
          addEmail(obj)
            .then((res) => {})
            .catch((err) => {
             
              console.log(err);
            });
          const filteredItems = requests.filter(
            (request) => request.expenseFormId !== idtoupd
          );
          const newItems = [...filteredItems, res];
          setRequests(newItems);
          setIdtoupd(null);
          setAct(null);
          setDecision(false);
          setApprovedAmount(0);
          setRemark("");
        })
        .catch((err) => {
          setIsSent(true)
          setOpenNoti(true);
          console.log(err);
        });
    } else if (decision && claimType === "medical") {
      let changedRequest = medicalRequests.find(
        (request) => request.medicalFormId === idtoupd
      );

      const obj = {
        receiverEmail: changedRequest.employee.email,
        subject: "",
        text: "",
      };

      if (act === 1) {
        changedRequest.progressStatus = "accepted by accountant";
        changedRequest.approvedAmount = approvedAmount;
        obj.subject = "Request accepted by accountant";
        obj.text = `payment now being processed for ${approvedAmount}`;
      } else {
        changedRequest.progressStatus = "rejected by accountant";
        changedRequest.remark = remark;
        obj.subject = "Request rejected by accountant";
        obj.text = `request rejected due to ${remark}`;
      }

      updateMedicalClaim(changedRequest)
        .then((res) => {
          addEmail(obj)
            .then((res) => {})
            .catch((err) => {
              console.log(err);
            });

          const filteredItems = medicalRequests.filter(
            (request) => request.medicalFormId !== idtoupd
          );
          const newItems = [...filteredItems, res];
          setMedicalRequests(newItems);
          setIdtoupd(null);
          setAct(null);
          setDecision(false);
          setApprovedAmount(0);
          setRemark("");
        })
        .catch((err) => {
          setIsSent(true)
          setOpenNoti(true);
          console.log(err);
        });
    }
  }, [decision]);
const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (
    <Styles>
      <div>
        {loading && <ClipLoader css={override} loading={loading} size={150} />}
 {isError && <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        ><Alert severity="error" style={{position:"absolute"}}>Something Has Happened While Fetching Data. Try Again</Alert></Snackbar>}
      {isSent && <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        ><Alert severity="error" style={{position:"absolute"}}>Something Has Happened While Updating Data. Try Again</Alert></Snackbar>}*
        <h3>Expense Claim</h3>
      </div>

      <ReqTable
        requests={requests}
        url="/accountant/requests"
        type="expense"
        changeStatus={changeStatus}
      />

      <div className="medical">
        <h3>Medical Claim</h3>
      </div>

      <ReqTable
        requests={medicalRequests}
        url="/accountant/requests"
        type="medical"
        changeStatus={changeStatus}
      />

      <Confirmpopup
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        act={act}
        setAct={setAct}
        decision={decision}
        setDecision={setDecision}
        approvedAmount={approvedAmount}
        setApprovedAmount={setApprovedAmount}
        remark={remark}
        setRemark={setRemark}
      ></Confirmpopup>
    </Styles>
  );
};

export default Requests;
