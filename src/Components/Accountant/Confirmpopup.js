import React from "react";
import {
  Button,
  Dialog,
  Grid,
  DialogContent,
  DialogTitle,
  TextField,
  DialogActions,
  DialogContentText,
} from "@material-ui/core";

export default function Confirmpopup(props) {
  const {
    openPopup,
    setOpenPopup,
    setDecision,
    act,
    approvedAmount,
    setApprovedAmount,
    remark,
    setRemark,
  } = props;
  // const { setDecision } = props;
  // const { act } = props;

  const handleSubmit = () => {
    setDecision(true);
    setOpenPopup(false);
  };

  const handleChange = (e) => {
    setApprovedAmount(e.target.value);
  };

  const handleRemark = (e) => {
    setRemark(e.target.value);
  };

  return (
    <Dialog open={openPopup}>
      <DialogTitle>
        <div>
          <Grid item xs={12} sm={12}>
            Confirmation
          </Grid>
        </div>
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          Are you sure you want to {act === 1 ? "accept" : "reject"} this
          request?
        </DialogContentText>
        {act !== 1 ? (
          <TextField
            label="Reason(s)"
            variant="outlined"
            rows={4}
            fullWidth
            value={remark}
            onChange={handleRemark}
          />
        ) : (
          <TextField
            label="Approved Amount"
            variant="outlined"
            rows={1}
            fullWidth
            value={approvedAmount}
            onChange={handleChange}
          />
        )}
      </DialogContent>
      <DialogActions>
        <Button
          style={{ marginLeft: "2rem" }}
          color="secondary"
          onClick={handleSubmit}
        >
          Confirm
        </Button>
        <Button
          style={{ marginLeft: "10rem" }}
          color="primary"
          onClick={() => setOpenPopup(false)}
        >
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
}
