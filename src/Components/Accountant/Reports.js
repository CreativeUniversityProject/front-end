// import React, { useContext } from "react";
// import { AccountantContext } from "../../context/accountantContext";
// import styled from "styled-components";
// import {
//   useTable,
//   useFilters,
//   useSortBy,
//   usePagination,
//   useGlobalFilter,
//   useAsyncDebounce,
// } from "react-table";
// import { matchSorter } from "match-sorter";
// import { Link } from "react-router-dom";

// const Styles = styled.div`
//   padding-top: 3rem;
//   margin-left: 2rem;

//   @media (min-width: 992px) {
//     margin-left: 16rem;
//   }
// `;

// const Reports = () => {
//   function GlobalFilter({
//     preGlobalFilteredRows,
//     globalFilter,
//     setGlobalFilter,
//   }) {
//     const count = preGlobalFilteredRows.length;
//     const [value, setValue] = React.useState(globalFilter);
//     const onChange = useAsyncDebounce((value) => {
//       setGlobalFilter(value || undefined);
//     }, 200);

//     return (
//       <span>
//         Search:{" "}
//         <input
//           value={value || ""}
//           onChange={(e) => {
//             setValue(e.target.value);
//             onChange(e.target.value);
//           }}
//           placeholder={`${count} records...`}
//           style={{
//             fontSize: "1.1rem",
//             border: "0",
//           }}
//         />
//       </span>
//     );
//   }

//   function DefaultColumnFilter({
//     column: { filterValue, preFilteredRows, setFilter },
//   }) {
//     const count = preFilteredRows.length;

//     return (
//       <input
//         value={filterValue || ""}
//         onChange={(e) => {
//           setFilter(e.target.value || undefined);
//         }}
//         placeholder={`Search ${count} records...`}
//       />
//     );
//   }

//   function SelectColumnFilter({
//     column: { filterValue, setFilter, preFilteredRows, id },
//   }) {
//     const options = React.useMemo(() => {
//       const options = new Set();
//       preFilteredRows.forEach((row) => {
//         options.add(row.values[id]);
//       });
//       return [...options.values()];
//     }, [id, preFilteredRows]);
//     return (
//       <select
//         value={filterValue}
//         onChange={(e) => {
//           setFilter(e.target.value || undefined);
//         }}
//       >
//         <option value="">All</option>
//         {options.map((option, i) => (
//           <option key={i} value={option}>
//             {option}
//           </option>
//         ))}
//       </select>
//     );
//   }

//   function fuzzyTextFilterFn(rows, id, filterValue) {
//     return matchSorter(rows, filterValue, { keys: [(row) => row.values[id]] });
//   }
//   fuzzyTextFilterFn.autoRemove = (val) => !val;

//   function filterGreaterThan(rows, id, filterValue) {
//     return rows.filter((row) => {
//       const rowValue = row.values[id];
//       return rowValue >= filterValue;
//     });
//   }

//   filterGreaterThan.autoRemove = (val) => typeof val !== "number";

//   const { requests, medicalRequests } = useContext(AccountantContext);

//   const medReports = medicalRequests
//     .filter((request) => request.progressStatus !== "accepted by tech lead")
//     .map(({ medicalFormId: formId, ...rest }) => ({
//       formId,
//       reqtype: "medical",
//       ...rest,
//     }));
//   const expReports = requests
//     .filter((request) => request.progressStatus !== "accepted by tech lead")
//     .map(({ expenseFormId: formId, ...rest }) => ({
//       formId,
//       reqtype: "expense",
//       ...rest,
//     }));

//   const mergedReports = expReports.concat(medReports);

//   const columns = React.useMemo(
//     () => [
//       {
//         Header: "Request No",
//         accessor: "formId",
//       },
//       {
//         Header: "Expense Type",
//         accessor: "reqtype",
//         Filter: SelectColumnFilter,
//         filter: "includes",
//       },
//       {
//         Header: "Form Description",
//         accessor: "formDescription",
//         Filter: SelectColumnFilter,
//         filter: "includes",
//       },
//       {
//         Header: "progressStatus",
//         accessor: "progressStatus",
//         Filter: SelectColumnFilter,
//         filter: "includes",
//       },
//       {
//         Header: "Employee",
//         accessor: "employee.firstName",
//       },
//       {
//         Header: "date",
//         accessor: "submittedDate",
//       },
//       {
//         Header: "amount",
//         accessor: "amount",
//       },
//     ],
//     []
//   );

//   const data = React.useMemo(() => mergedReports, []);

//   const filterTypes = React.useMemo(
//     () => ({
//       fuzzyText: fuzzyTextFilterFn,
//       text: (rows, id, filterValue) => {
//         return rows.filter((row) => {
//           const rowValue = row.values[id];
//           return rowValue !== undefined
//             ? String(rowValue)
//                 .toLowerCase()
//                 .startsWith(String(filterValue).toLowerCase())
//             : true;
//         });
//       },
//     }),
//     []
//   );

//   const defaultColumn = React.useMemo(
//     () => ({
//       Filter: DefaultColumnFilter,
//     }),
//     []
//   );

//   const {
//     getTableProps,
//     getTableBodyProps,
//     headerGroups,
//     page,
//     prepareRow,
//     canPreviousPage,
//     canNextPage,
//     pageOptions,
//     pageCount,
//     gotoPage,
//     nextPage,
//     previousPage,
//     setPageSize,
//     state,
//     state: { pageIndex, pageSize },
//     visibleColumns,
//     preGlobalFilteredRows,
//     setGlobalFilter,
//   } = useTable(
//     {
//       columns,
//       data,
//       initialState: { pageIndex: 0 },
//       defaultColumn,
//       filterTypes,
//     },
//     useFilters,
//     useGlobalFilter,
//     useSortBy,
//     usePagination
//   );

//   return (
//     <Styles>
//       <div>
//         <h2>Reports</h2>
//       </div>
//       <table {...getTableProps()} className="table table-bordered">
//         <thead>
//           {headerGroups.map((headerGroup) => (
//             <tr {...headerGroup.getHeaderGroupProps()}>
//               {headerGroup.headers.map((column) => (
//                 <th>
//                   {column.render("Header")}
//                   <div>{column.canFilter ? column.render("Filter") : null}</div>
//                 </th>
//               ))}
//               <th>actions</th>
//             </tr>
//           ))}
//           <tr>
//             <th
//               colSpan={visibleColumns.length}
//               style={{
//                 textAlign: "left",
//               }}
//             >
//               <GlobalFilter
//                 preGlobalFilteredRows={preGlobalFilteredRows}
//                 globalFilter={state.globalFilter}
//                 setGlobalFilter={setGlobalFilter}
//               />
//             </th>
//           </tr>
//         </thead>
//         <tbody {...getTableBodyProps()}>
//           {page.map((row, i) => {
//             prepareRow(row);
//             return (
//               <tr {...row.getRowProps()} scope="row">
//                 {row.cells.map((cell) => {
//                   return (
//                     <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
//                   );
//                 })}
//                 <td>
//                   <Link to={`/accountant/reports/${row.original.formId}`}>
//                     <i className="fas fa-mouse-pointer text-danger sI"></i>
//                   </Link>
//                 </td>
//               </tr>
//             );
//           })}
//         </tbody>
//       </table>

//       <div className="pagination">
//         <button
//           onClick={() => gotoPage(0)}
//           disabled={!canPreviousPage}
//           className="page-link"
//         >
//           {"<<"}
//         </button>{" "}
//         <button
//           onClick={() => previousPage()}
//           disabled={!canPreviousPage}
//           className="page-link"
//         >
//           {"<"}
//         </button>{" "}
//         <button
//           onClick={() => nextPage()}
//           disabled={!canNextPage}
//           className="page-link"
//         >
//           {">"}
//         </button>{" "}
//         <button
//           onClick={() => gotoPage(pageCount - 1)}
//           disabled={!canNextPage}
//           className="page-link"
//         >
//           {">>"}
//         </button>{" "}
//         <span className="page-link">
//           Page{" "}
//           <strong>
//             {pageIndex + 1} of {pageOptions.length}
//           </strong>{" "}
//         </span>
//         <span className="page-link">
//           | Go to page:{" "}
//           <input
//             type="number"
//             defaultValue={pageIndex + 1}
//             onChange={(e) => {
//               const page = e.target.value ? Number(e.target.value) - 1 : 0;
//               gotoPage(page);
//             }}
//             style={{ width: "100px" }}
//           />
//         </span>{" "}
//         <select
//           className="page-link"
//           value={pageSize}
//           onChange={(e) => {
//             setPageSize(Number(e.target.value));
//           }}
//         >
//           {[10, 20, 30, 50, 100].map((pageSize) => (
//             <option key={pageSize} value={pageSize}>
//               Show {pageSize}
//             </option>
//           ))}
//         </select>
//       </div>
//     </Styles>
//   );
// };

// export default Reports;

import { React, useState, useContext } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { TablePagination, TableSortLabel, TextField } from "@material-ui/core";
import { LibraryBooksRounded, SearchRounded } from "@material-ui/icons";
import InputAdornment from "@material-ui/core/InputAdornment";
import { AccountantContext } from "../../context/accountantContext";
import { Link } from "react-router-dom";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#EC2329",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

// function createData(reporttitle, expensetype, date, amount, status) {
//   return { reporttitle, expensetype, date, amount, status };
// }
function createData(
  formId,
  reqtype,
  formDescription,
  progressStatus,
  firstName,
  amount,
  appamount
) {
  return {
    formId,
    reqtype,
    formDescription,
    progressStatus,
    firstName,
    amount,
    appamount,
  };
}

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 700,
    width: "99%",
  },
  main: {
    marginLeft: "16rem",
    marginTop: "1rem",

    [theme.breakpoints.down("sm")]: {
      marginLeft: "0rem",
    },
  },
}));

export default function Reports() {
  const { requests, medicalRequests } = useContext(AccountantContext);

  const medReports = medicalRequests
    .filter((request) => request.progressStatus !== "accepted by tech lead")
    .map(({ medicalFormId: formId, ...rest }) => ({
      formId,
      reqtype: "medical",
      ...rest,
    }));
  const expReports = requests
    .filter((request) => request.progressStatus !== "accepted by tech lead")
    .map(({ expenseFormId: formId, ...rest }) => ({
      formId,
      reqtype: "expense",
      ...rest,
    }));

  const mergedReports = expReports.concat(medReports);

  const rows = mergedReports.map((report) =>
    createData(
      report.formId,
      report.reqtype,
      report.formDescription,
      report.progressStatus,
      report.employee.firstName,
      report.amount,
      report.approvedAmount
    )
  );

  // createData("Fever", "medical", "2021-05-12", "3000", "Approved"),
  // createData("fracture", "expense", "2021-04-12", "43", "Approved"),
  // createData("vomit", "medical", "2023-04-12", "432", "Approved"),
  // createData("tavel", "expense", "2021-04-12", "3545", "rejected"),
  // createData("Food", "expense", "2020-04-12", "3300", "Approved"),
  // createData("taxi", "expense", "2020-04-12"," 500", "Approved"),
  // createData("corona", "medical", "2020-08-12", "4200", "rejected"),

  const headCells = [
    // { id: "reporttitle", lable: "Report title" },
    // { id: "expensetype", lable: "Expense type" },
    // { id: "date", lable: "Submitted date" },
    // { id: "amount", lable: "Amount" },
    // { id: "status", lable: "status" },
    { id: "formId", lable: "Form ID" },
    { id: "reqtype", lable: "Request type" },
    { id: "formDescription", lable: "Form Description" },

    { id: "firstName", lable: "FirstName" },
    { id: "progressStatus", lable: "Progress status" },
    { id: "amount", lable: "Amount" },
    { id: "appovedAmount", lable: "Approved Amount" },
    { id: "action", lable: "Action" },
  ];

  const pages = [5, 10, 15];
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(pages[page]);
  const [order, setOrder] = useState();
  const [orderby, setOrderBy] = useState();
  const [filterFn, setFilterFn] = useState({
    fn: (items) => {
      return items;
    },
  });

  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  function getComparator(order, orderby) {
    return order === "desc"
      ? (a, b) => descendingComparator(a, b, orderby)
      : (a, b) => -descendingComparator(a, b, orderby);
  }

  function descendingComparator(a, b, orderby) {
    if (b[orderby] < a[orderby]) {
      return -1;
    }
    if (b[orderby] > a[orderby]) {
      return 1;
    }
    return 0;
  }

  const recordsAfterPagingAndSorting = () => {
    return stableSort(filterFn.fn(rows), getComparator(order, orderby)).slice(
      page * rowsPerPage,
      (page + 1) * rowsPerPage
    );
  };

  const handleSortRequest = (cellid) => {
    const isAsc = orderby === cellid && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(cellid);
  };

  const handleSearch = (e) => {
    let target = e.target;
    setFilterFn({
      fn: (items) => {
        if (target.value == "") return items;
        else
          return items.filter(
            (x) =>
              // x.formId.includes(target.value)||
              x.reqtype.toLowerCase().includes(target.value) ||
              x.formDescription.toLowerCase().includes(target.value) ||
              x.firstName.toLowerCase().includes(target.value) ||
              x.progressStatus.toLowerCase().includes(target.value)
          );
      },
    });
  };

  return (
    <div className={classes.main}>
      <TextField
        label="Search"
        onChange={handleSearch}
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchRounded />
            </InputAdornment>
          ),
        }}
        style={{ paddingBottom: "20px" }}
      />

      <TableContainer>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              {headCells.map((headcell) => (
                <StyledTableCell
                  align="left"
                  key={headcell.id}
                  sortDirection={orderby === headcell.id ? order : false}
                >
                  <TableSortLabel
                    active={orderby === headcell.id}
                    direction={orderby === headcell.id ? order : "asc"}
                    onClick={() => {
                      handleSortRequest(headcell.id);
                    }}
                  >
                    {headcell.lable}
                  </TableSortLabel>
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {recordsAfterPagingAndSorting().map((row) => (
              <StyledTableRow key={row.formId}>
                <StyledTableCell align="left">{row.formId}</StyledTableCell>
                <StyledTableCell align="left">{row.reqtype}</StyledTableCell>
                <StyledTableCell align="left">
                  {row.formDescription}
                </StyledTableCell>

                <StyledTableCell align="left">{row.firstName}</StyledTableCell>
                <StyledTableCell align="left">
                  {row.progressStatus}
                </StyledTableCell>
                <StyledTableCell align="left">{row.amount}</StyledTableCell>
                <StyledTableCell align="left">{row.appamount}</StyledTableCell>
                <StyledTableCell align="center">
                  <Link to={`/accountant/reports/${row.formId}`}>
                    {/* <i className="fas fa-mouse-pointer text-danger sI"></i>                   */}
                    <LibraryBooksRounded color="secondary" />
                  </Link>
                </StyledTableCell>
              </StyledTableRow>

              //     <StyledTableRow key={row.reporttitle}>
              // <StyledTableCell component="th" scope="row">
              //      {row.reporttitle}
              //  </StyledTableCell>
              //      <StyledTableCell align="left">
              //      {row.expensetype}
              //       </StyledTableCell>
              //     <StyledTableCell align="left">{row.date}</StyledTableCell>
              //        <StyledTableCell align="left">{row.amount}</StyledTableCell>
              //        <StyledTableCell align="left">{row.status}</StyledTableCell>
              //     </StyledTableRow>
            ))}
          </TableBody>
        </Table>
        <TablePagination
          component="div"
          page={page}
          rowsPerPageOptions={pages}
          rowsPerPage={rowsPerPage}
          count={rows.length}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </TableContainer>
    </div>
  );
}
