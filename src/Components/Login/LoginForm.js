import React, { Component, useContext, useState } from "react";
import { Button } from "react-bootstrap";
import "../Login/LoginForm.css";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {makeStyles} from '@material-ui/core/styles';
//import { useHistory } from 'react-router-dom';
//import {Link} from "react-router-dom";
//import {Redirect} from 'react-router-dom';
import { Link, useHistory } from "react-router-dom";
import { GlobalContext } from "../../context/GlobalContext";
import { signInUser } from "../../services/auth/authToken";
import HelpIcon from '@material-ui/icons/Help';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import ClipLoader from "react-spinners/ClipLoader";
import { css } from "@emotion/core";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

const override = css`

position: absolute;
  top: calc(50% - 4em);
  left: calc(50% - 4em);
  width: 9em;
  height: 9em;
  border: 1.1em solid rgba(252,28, 20, 0.2);
  border-left: 1.1em solid #f50800;
  border-radius: 50%;
  
  
`;


const useStyles = makeStyles(theme => ({
 
 button:{

 
  [theme.breakpoints.down('xs')]: {
    marginLeft:"7rem"
  },
   background:"#9F9F9F",
   borderColor:"white"

 },
 typography: {
  padding: theme.spacing(2),
  
},
buttonText:{
  color:"black",
  // fontWeight:"bold"
}

 
}))

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
export const LoginForm = (props) => {
 
  const classes = useStyles()
  const history = useHistory();

  const [{ email, password }, setState] = useState({
    email: "",
    password: "",
  });
  const [openNoti, setOpenNoti] = useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [loading, setLoading] = useState(false);
 const [isError, setIsError] = useState(false);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;


  const onSubmit = async (e) => {
    e.preventDefault();
    setLoading(true)

    try {
      const user = await signInUser({ email, password });
            console.log(user)
   //   dispatch(signIn(user));
           localStorage.setItem("role",'emp')
               setLoading(false);
      history.replace({ pathname: "/select" });
    } catch (error) {
      setOpenNoti(true);
     setIsError(true);
      setLoading(false);
    }
  
  };

  const onChange = (e) => {
    const { name, value } = e.target;

    setState((prevState) => ({ ...prevState, [name]: value }));
  };
 const handleClose1 = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };
  return (

   

    <div className="bak">
 {isError && <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose1}
        ><Alert  onClose={handleClose1} severity="error" >Invalid Credentials. Try Again</Alert></Snackbar>}
{loading && <ClipLoader css={override}  loading={loading}  size={150} />}

      <div className="flex-container">
        <div className="logo-container">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
        </div>

        
        {/* <Divider
                orientation="vertical"
                style={{ color: "black", width: "2px", height: '60%' }}
              /> */}

        <div>
          <form className="form">
            <h1 className="h1">T Y C H E</h1>
            <br />
            <br />
            <h3>Welcome to Tyche</h3>
            <br />

            <Grid item>
              <TextField
                label="Username"
                name="email"
                type="text"
                variant="standard"
                value={email}
                onChange={onChange}
                placeholder="Enter email address"
                required
              />
            </Grid>

            <br />
            <Grid item>
              <TextField
                label="Password"
                name="password"
                type="password"
                variant="standard"
                value={password}
                onChange={onChange}
                placeholder="Enter password"
                required
              />   
            </Grid>
           
            <br />

            <Button variant="contained" 
              className={classes.button}
              disabled={loading}
              onClick={onSubmit }
            >
              <Typography className={classes.buttonText}>
              Login 
              </Typography>
            </Button>
            &nbsp;      <HelpOutlineIcon  style={{ color: "white" }} 
                            aria-describedby={id}
                            onClick={handleClick}
            />

<Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <Typography className={classes.typography}>Welcome to TYCHE. <br/> 
        If you forget your password please contact admin</Typography>
      </Popover>
            <div >
           

            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
