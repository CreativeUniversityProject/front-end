import React, { useState, useEffect } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import { makeStyles } from "@material-ui/styles";
import logo2 from "../assets/logo2.svg";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import NotificationsIcon from "@material-ui/icons/Notifications";
import VisibilityIcon from "@material-ui/icons/Visibility";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import MenuItem from "@material-ui/core/MenuItem";
import SettingsIcon from "@material-ui/icons/Settings";
import { Link, useHistory } from "react-router-dom";
import { getProfileForEmployee } from "../services/profilePicture";
import Alert from '@material-ui/lab/Alert'
// import { useTheme } from "@material-ui/core/styles";

import MenuIcon from "@material-ui/icons/Menu";
import MenuList from "@material-ui/core/MenuList";

import { ContextHead } from "../context/ContextHead";
import { Menu } from "@material-ui/core";
// import { GlobalContext } from "../context/GlobalContext";

function ElevationScroll(props) {
  const { children } = props;

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

const useStyles = makeStyles((theme) => ({
  toolbarMargin: {
    ...theme.mixins.toolbar,
  },

  appbar: {
    background: "#ec2329",
    zIndex: theme.zIndex.modal + 1,
  },
  logo: {
    height: "3em",
  },

  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    background: theme.palette.background.paper,
    color: "black",
  },

  grow: {
    flexGrow: 1,
  },

  root: {
    display: "flex",
  },

  menu: {
    marginLeft: "auto",
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
  typo: {
    fontFamily: "Raleway",
    fontWeight: 700,
    textTransform: "capitalize",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  listtext: {
    color: "black",
  },

  menu1: {
    color: "black",

    "&:hover": {
      Color: "white",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  menuitem: {
    ...theme.typography.tab,

    "&:hover": {
      backgroundColor: "#EC2329",
      color: "white",
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),

    [theme.breakpoints.up("md")]: {
      display: "none",
    },
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
}));

export default function Header() {
  const history = useHistory();

  const onLogOut = (e) => {
    e.preventDefault();
    history.replace({ pathname: "/" });
    //dispatch(logOut());
    localStorage.clear();
  };
  const classes = useStyles();
const [isError,setIsError]=useState(false)
  const [anchorEl, SetAnchorEl] = React.useState(null);
  const [anchorElSub, SetAnchorElSub] = React.useState(null);
  const [anchorElSub1, SetAnchorElSub1] = React.useState(null);

  const { mobileOpen } = React.useContext(ContextHead);
  const [mobileOpen1, setMobileOpen1] = mobileOpen;

  const [image, setImage] = useState("");

  const userDetails = JSON.parse(localStorage.getItem("user"));
  const role = userDetails.userRole.roleName;

  const handleClick = (e) => {
    SetAnchorEl(e.currentTarget);
  };
  const handleClickSub = (e) => {
    SetAnchorElSub(e.currentTarget);
  };
  const handleClickSub1 = (e) => {
    SetAnchorElSub1(e.currentTarget);
  };

  const handleClose = (e) => {
    SetAnchorEl(null);

    SetAnchorElSub(null);
    SetAnchorElSub1(null);
  };

  useEffect(async () =>{
    getProfileForEmployee(userDetails.employee.employeeId)
      .then((res) => {
        setImage(res);

        console.log(image);
      })
      .catch((err) => {
        setIsError(true)
        console.log(err);
      });
  }, []);

  return (
    <div className={classes.root}>
     
      <ElevationScroll>
        <AppBar position="fixed" className={classes.appbar} color="primary">
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={() => setMobileOpen1(!mobileOpen1)}
              className={classes.menuButton}
              style={{ outline: "none" }}
            >
              <MenuIcon />
            </IconButton>

            <img className={classes.logo} alt="companylogo" src={logo2} />

            <div className={classes.grow} />
{/*{isError && <Alert severity="error" style={{position:"absolute"}}>Something Has Happened While Fetching Profile Picture. Try Again</Alert>}*/}
            <IconButton
              color="inherit"
              aria-controls="simple-menu"
              aria-haspopup="true"
              onClick={(event) => handleClick(event)}
              style={{ outline: "none" }}
            >
              <Typography className={classes.typo}>
                {userDetails.employee.firstName}
                <span>&nbsp; &nbsp;</span>{" "}
              </Typography>
              <Avatar className={classes.avatar} src={image.profilePicture} />
              <ArrowDropDownIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
      </ElevationScroll>

      <Menu
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        id="simple-menu"
        classes={{ paper: classes.menu1 }}
        onClose={handleClose}
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
      >
        <MenuList style={{ outline: "none" }}>
          {/* <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            component={Link}
            to="/employee"
          >
            <SupervisorAccountIcon /> <span>&nbsp; &nbsp;</span> Profile{" "}
          </MenuItem> */}

          <MenuItem
            aria-controls="simple"
            aria-haspopup="true"
            onClick={handleClickSub}
            classes={{ root: classes.menuitem }}
          >
            {" "}
            <VisibilityIcon /> <span>&nbsp; &nbsp;</span> view As{" "}
          </MenuItem>

          {/* <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            component={Link}
            to={`/employee/changePassword/${userDetails.employee.employeeId}`}
          >
            {" "}
            <SettingsIcon /> <span>&nbsp; &nbsp;</span> Settings{" "}
          </MenuItem> */}

          <MenuItem
            aria-controls="simple"
            aria-haspopup="true"
            onClick={handleClickSub1}
            classes={{ root: classes.menuitem }}
          >
            {" "}
            <SettingsIcon /> <span>&nbsp; &nbsp;</span> Settings{" "}
          </MenuItem>

          <MenuItem classes={{ root: classes.menuitem }} onClick={handleClose}
          component={Link}
          to="/help">
            {" "}
            <HelpOutlineIcon /> <span>&nbsp; &nbsp;</span> Help{" "}
          </MenuItem>

          <MenuItem classes={{ root: classes.menuitem }} onClick={onLogOut}>
            {" "}
            <ExitToAppIcon /> <span>&nbsp; &nbsp;</span> Logout{" "}
          </MenuItem>
        </MenuList>
      </Menu>

      <Menu
        anchorEl={anchorElSub}
        classes={{ paper: classes.menu1 }}
        id="simple"
        open={Boolean(anchorElSub)}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        onClick={handleClose}
      >
        <MenuList style={{ outline: "none" }}>
          <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            onClick={() => localStorage.setItem("role", "emp")}
            component={Link}
            to="/select"
          >
            {" "}
            Employee{" "}
          </MenuItem>

          <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            onClick={() => localStorage.setItem("role", "tech")}
            component={Link}
            to="/techlead/requests"
            hidden={role == "ACCOUNTANT" || role == "ADMIN" || role == "USER"}
          >
            {" "}
            TechLead{" "}
          </MenuItem>

          <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            onClick={() => localStorage.setItem("role", "acc")}
            component={Link}
            to="/accountant/requests"
            hidden={role == "TECHLEAD" || role == "ADMIN" || role == "USER"}
          >
            {" "}
            Accountant{" "}
          </MenuItem>

          <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            onClick={() => localStorage.setItem("role", "admin")}
            component={Link}
            to="/admin"
            hidden={
              role == "ACCOUNTANT" || role == "TECHLEAD" || role == "USER"
            }
          >
            {" "}
            Admin{" "}
          </MenuItem>
        </MenuList>
      </Menu>

      <Menu
        anchorEl={anchorElSub1}
        classes={{ paper: classes.menu1 }}
        id="simple"
        open={Boolean(anchorElSub1)}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        onClick={handleClose}
      >
        <MenuList style={{ outline: "none" }}>
          <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            component={Link}
            to={`/employee/changePassword/${userDetails.employee.employeeId}`}
          >
            Change Password
          </MenuItem>
        </MenuList>
      </Menu>

      <div className={classes.toolbarMargin} />
    </div>
  );
}
