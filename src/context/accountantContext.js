import React, { createContext, useState, useEffect } from "react";
import { getAllExpenseClaims } from "../services/ExpenseForm";
import { getAllMedicalClaims } from "../services/MedicalForm";

export const AccountantContext = createContext();

export const AccountantProvider = (props) => {
  const [requests, setRequests] = useState([]);
  const [medicalRequests, setMedicalRequests] = useState([]);

  return (
    <AccountantContext.Provider
      value={{ requests, setRequests, medicalRequests, setMedicalRequests }}
    >
      {props.children}
    </AccountantContext.Provider>
  );
};
