
import React,{createContext, useState,useEffect} from 'react'
import { getEmployee } from "../services/employee";


export const EmpContext = createContext();

export const EmpProvider= props =>{
     const[user,setUser]= useState([]) ;

const userDetails1=JSON.parse(localStorage.getItem('user'))

 useEffect(async () => {
    getEmployee(userDetails1.employee.employeeId)
      .then((res) => {
        console.log("response");
        console.log(res);
        setUser(res);
        console.log(user)
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
    
    
return(
       <EmpContext.Provider value={[user,setUser]}>
           {props.children}
       </EmpContext.Provider>
    )
      
       

}