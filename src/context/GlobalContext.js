import React, { createContext, useReducer } from "react";
import AppReducer from "../services/auth/authReducer";
import actions from "../services/auth/authActions";


// Initial State
const initialState = {
  
 userDetails:{},
  loggedInUser: false,
 
};

// Create Context
export const GlobalContext = createContext(initialState);

// Provider Component
export const GlobalProvider = ({ children }) => {
  const [{host, userDetails,loggedInUser }, dispatch] = useReducer(AppReducer, initialState);


  return (
    <GlobalContext.Provider value={{ host,userDetails, loggedInUser, dispatch, ...actions }}>{children}</GlobalContext.Provider>
  );
};
