import React, { createContext, useState, useEffect } from "react";
import {
  getUsers,
  getDesignations,
  getDepartments,
  getRoles,
} from "../services/user";

export const AdminContext = createContext();

export const AdminProvider = (props) => {
  const [employees, setEmployees] = useState([]);
  const [designations, setDesignations] = useState([]);
  const [departments, setDepartments] = useState([]);
  const [roles, setRoles] = useState([]);

  // useEffect(async () => {
  //   // getUsers()
  //   //   .then((res) => {
  //   //     setEmployees(res);
  //   //     getDesignations()
  //   //       .then((res) => {
  //   //         setDesignations(res);
  //   //         getDepartments()
  //   //           .then((res) => {
  //   //             setDepartments(res);
  //   //             getRoles()
  //   //               .then((res) => {
  //   //                 setRoles(res);
  //   //               })
  //   //               .catch((err) => {
  //   //                 console.log(err);
  //   //               });
  //   //           })
  //   //           .catch((err) => {
  //   //             console.log(err);
  //   //           });
  //   //       })
  //   //       .catch((err) => {
  //   //         console.log(err);
  //   //       });
  //   //   })
  //   //   .catch((err) => {
  //   //     console.log(err);
  //   //   });

  //   getDesignations()
  //     .then((res) => {
  //       setDesignations(res);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });

  //   getUsers()
  //     .then((res) => {
  //       setEmployees(res);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });

  //   getDepartments()
  //     .then((res) => {
  //       setDepartments(res);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });

  //   getRoles()
  //     .then((res) => {
  //       setRoles(res);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // }, []);

  // console.log("emp");
  // console.log(employees);
  // console.log("dept");
  // console.log(departments);
  // console.log("desig");
  // console.log(designations);
  // console.log("roles");
  // console.log(roles);

  return (
    <AdminContext.Provider
      value={{
        employees,
        setEmployees,
        departments,
        setDepartments,
        designations,
        setDesignations,
        roles,
        setRoles,
      }}
    >
      {props.children}
    </AdminContext.Provider>
  );
};
