import React, { createContext, useState, useEffect } from "react";
import { getExpenseClaims } from "../services/ExpenseForm";
import { getMedicalClaims } from "../services/MedicalForm";

export const RequestContext = createContext();

export const RequestProvider = (props) => {
  const [requests, setRequests] = useState([]);
  const [medicalRequests, setMedicalRequests] = useState([]);
  const [Acc, setAcc] = useState([]);

  return (
    <RequestContext.Provider
      value={{ requests, setRequests, medicalRequests, setMedicalRequests,Acc,setAcc}}
    >
      {props.children}
    </RequestContext.Provider>
  );
};
