import React, { createContext, useState, useEffect } from "react";
import { getExpenseClaimsEmp } from "../services/ExpenseForm";
import { getMedicalClaimsEmp } from "../services/MedicalForm";

export const ContextReport = createContext();

export const ReportProvider = (props) => {
  const [ERepo, setERepo] = useState([]);
  const [MRepo, setMRepo] = useState([]);

  return (
    <ContextReport.Provider
      value={{
        ERepo,
        setERepo,
        MRepo,
        setMRepo,
      }}
    >
      {props.children}
    </ContextReport.Provider>
  );
};
