import React, { createContext, useState } from "react";

export const ContextHead = createContext();

export const MovieProvider = (props) => {
  const [mobileOpen, setMobileOpen] = useState(false);

  const [userPermission1, setuserPermission1] = useState("emp");
  //tech
  //admin
  //acc

  return (
    <ContextHead.Provider
      value={{
        mobileOpen: [mobileOpen, setMobileOpen],
        userPermission: [userPermission1, setuserPermission1],
      }}
    >
      {props.children}
    </ContextHead.Provider>
  );
};
