import React from "react";
import bg from "./assets/bg.jpg";
import "./err.css";
import {Link } from "react-router-dom";

function Err() {
  return (
    <div id="notfound">
		<div className="notfound-bg"></div>
		<div className="notfound">
			<div className="notfound-404">
				<h1>404</h1>
			</div>
			<h2>we are sorry</h2>
			<h3>The link you followed may be broken</h3>
			<button class="home-btn"><Link to={"/select"}>Go Home</Link></button>
			
			
		</div>
	</div>

  );
}

export default Err
