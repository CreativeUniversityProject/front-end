import { React } from "react";

import Footer from "../Components/Footer";

import Report from "../Components/Employee/Report";
import { ReportProvider } from "../context/ContextReport";

import { makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  pageContent: {
    marginLeft: "17rem",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "1rem",
    },
  },
  typo: {
    marginTop:"1rem",
    marginLeft: "17rem",
    fontSize:"1.8em",
    fontWeight:"bold",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "1rem",
    
    },
  },
}));

export default function EmpReportpage() {
  const classes = useStyles();
  return (
    <div>
    
      <Typography   className={classes.typo}>Employee Full Report </Typography>
      <br />
      
      <div className={classes.pageContent}>
        <Report />
      </div>
    
      <Footer />
    </div>
  );
}
