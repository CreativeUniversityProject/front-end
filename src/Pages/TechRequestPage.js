import React from "react";
import Requests from "../Components/TechLead/Requests";
import { MovieProvider } from "../context/ContextHead";
import Footer from "../Components/Footer";
import Header from "../Components/Header";
import SideBar from "../Components/SideBar";

export default function TechRequestPage() {
  return (
    <div>
      {/* <MovieProvider>
           <Header  />
            <SideBar/>
            </MovieProvider> */}

      <Requests />

      <Footer />
    </div>
  );
}
