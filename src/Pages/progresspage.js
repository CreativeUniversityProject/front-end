import React from "react";
import ExpenseProgress from "../Components/Employee/ExpenseProgress";
import MedicalProgress from "../Components/Employee/MedicalProgress";

import Footer from "../Components/Footer";


export default function progress() {



  return (
    <div  style={{marginTop:"1rem"}}>
      {/* <ReportProvider> */}

      <ExpenseProgress />

      <br />

      <MedicalProgress />

      <Footer />
      {/* </ReportProvider>      */}
    </div>
  );
}
