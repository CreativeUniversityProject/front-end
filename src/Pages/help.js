import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import styled from "styled-components";





export default function Help() {
  return (
    <Styles>
  
        <div className="col-6"  >
          <h1>Tyche</h1>
          <h6>A cloud based reimbursement web application to request and report all your reimbursements.</h6>
        </div>
        <br/>
          <div className="back">

            
            <h3>Browser Requirements</h3>
            <ul>
              <p className="si"><li>Use the Latest version of web browser to get the best expense reporting experience.</li></p></ul>
            
              
             
            </div><br/>
            <div className="back">
            <div className="row" ><dl><dt>
              <h3>User guide</h3>
              </dt>
              <dd><p className="si">Employee<br/>
              <ul>
              <li>Submit reimbursement forms</li>
              <li>View and edit profile</li>
              <li>View reports and reimbursement progress status</li></ul>
              
Techlead and Accountant	
<ul>
<li>View employee requests</li>
<li>	Accept/Reject employee request</li>
<li>View Reports</li>
</ul>
Admin<br/>
<ul>
<li>Add, edit, delete, view employee details</li> 
<li>Assign user roles</li>
<li>	Add dependents to the employee</li>

</ul></p></dd></dl>
            </div>
            
            </div><br/>

            <div className="back">
            <div className="row">
            <h3> Login</h3>
              <p className="si"><ul><li>If you are already a user, you can sign in with your email and password.</li>
              <li>If you are a new user, you can contact admin to add you to the system. After that you can sign in with your email and the default password ‘test’ and later you can change the password</li>
</ul></p>
              
              </div>
            </div><br/>
            <div className="back">
            <div className="row">
              <h3>Header and Sidebar</h3>
              <p className="si"><ul><li>You can change your user role from the drop down list in the header and navigate through the pages using sidebar</li></ul></p>
              
              </div>
            </div><br/>
            
            <div className="row">
              <div className="col-6">For further details contact the administration office</div>
              
              
            </div><br/>
     
    </Styles>
  );
}






const Styles = styled.div`
  padding-top: 2rem;
  margin-left: 17rem;
  //background-color: #dfe1ee;



  .row {
    margin-bottom: 1rem;
  }


.si{

  font-size: 20px;


}
  
  .col-6{
    
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),0 6px 20px 0 rgba(0, 0, 0, 0.19);
    margin-left: auto;
    margin-right: auto;
    border-radius: 25px 25px;
    font-size: 20px;
    padding: 3%;
    font-weight: 900;
    color: #fff;
   text-align:center;
   font-variant: small-caps;
    background-color: #ec2329;
    font-weight: bold;
  }
  .col-7 {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    margin-left: 6rem;
    margin-right: auto;
    border-radius: 1.5rem;
    width: 50%;
    padding: 1%;
    font-weight: 900;
    color: #fff;
    text-align: center;
    // background-color: #3b5998;
    background-color: #ec2329;

    font-weight: bolder;
  }

  .col-2 {

    font-weight: bold;
    color: #000;
   
    
   
  }
  .col-10 {
    font-weight: bold;
    font-weight: 900;
    color: #000;
    margin-bottom: 1rem;
  }
  .im {
    padding: 1%;
    margin-bottom: 3%;
    border-radius: 1.5rem;
    width: 80%;
    height: 100%;
  }
  .back {
    background-color: #fff;
    
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2),
      0 6px 20px 0 rgba(0, 0, 0, 0.19);
    color: #000;
    opacity: 0.8;
    border-radius: 1.5rem;
    //width: 80%;
   // height: 100%;
    padding: 3%;
   // margin-bottom: 3%;
   // margin-right: auto;
  }
  .back2 {
    background-color: #fff;
    opacity: 0.8;
    border-radius: 1.5rem;
    width: 80%;
    height: 100%;
    padding: 3%;
    margin-bottom: 3%;
    margin-left: -6rem;
  }
`;
