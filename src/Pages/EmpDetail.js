import React from 'react'

import Footer from "../Components/Footer"

import EmpDetails from "../Components/Employee/EmpDetails"
import {EmpProvider} from '../context/ContextEmp'

export default function EmpDetail() {
    return (
        <div style={{marginTop:"1rem"}}>

{/* 
<MovieProvider>
           <Header  />
            <SideBar/>
            </MovieProvider> */}
            <EmpProvider>
<EmpDetails/>
</EmpProvider>
            <Footer/>
            
        </div>
    )
}
