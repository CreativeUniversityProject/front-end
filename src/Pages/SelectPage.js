import React from 'react'

import MedicalTile from "../Components/selection/MedicalTile"
import ExpenseTile from "../Components/selection/ExpenseTile"

import Footer from "../Components/Footer"

import Box from '@material-ui/core/Box';

export default function SelectPage() {
    return (
        <div >
            
            <Box display="flex" flexDirection="row" p={1} m={1} 
                
                flexWrap="wrap"
                marginLeft="25rem" 
        
                marginTop="3rem"
                >
           
          <Box item  >
          <MedicalTile/>
          </Box>
                    
          <Box item  >
          <ExpenseTile/>
          </Box>
          </Box>

             <Footer/>
        </div>
    )
}
