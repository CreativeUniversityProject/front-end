import React, { Component } from "react";
import "./App.css";

//import Grid from "@material-ui/core/Grid";

import { ThemeProvider } from "@material-ui/styles";
import Select from "./Pages/SelectPage";
import theme from "./Components/Theme";

import { BrowserRouter, Redirect, Route } from "react-router-dom";
import ExpenseForm from "./Pages/ExpFormPage";

//techlead
import { RequestProvider } from "./context/techLeadContext";
import Requests from "./Pages/TechRequestPage";
import Reports from "./Pages/TechReportPage";
import RequestDetails from "./Components/TechLead/RequestDetails";
import ReportsDetails from "./Components/TechLead/ReportsDetails";

import Switch from "react-bootstrap/esm/Switch";
import MedicalForm from "./Pages/MedicalFormPage";

import empdetails from "./Pages/EmpDetail";
import editProfile from "./Components/Employee/EditProfile";
import report from "./Pages/EmpReportpage";

import { AdminProvider } from "./context/adminContext";
import AddUser from "./Components/Admin/AddUser";
import Admin from "./Components/Admin/Admin";
import EditUser from "./Components/Admin/EditUser";
import ViewUser from "./Components/Admin/ViewUser";

//Accoutant
import { AccountantProvider } from "./context/accountantContext";
import AccountantRequests from "./Components/Accountant/Requests";
import AccountantReports from "./Components/Accountant/Reports";
import AccountantRequestDetails from "./Components/Accountant/RequestDetails";
import AccountantReportsDetails from "./Components/Accountant/ReportsDetails";

// import Header from "./Components/Header";
import progress from "./Pages/progresspage";
import AddDependent from "./Components/Admin/AddDependent";
// import Admin from "./Components/Admin/Admin";
import login from "./Pages/LoginPage.js";
// import SideBar from "./Components/SideBar";
// import { MovieProvider } from "./context/ContextHead";

// import Requests from "./Pages/TechRequestPage";
// import Reports from "./Pages/TechReportPage";
// import RequestDetails from "./Components/TechLead/RequestDetails";

// import AccRequests from "./Pages/AccRequestPage";
// import AccReports from "./Pages/AccReportPage";
// import AccRequestDetails from "./Components/Accountant/AccRequestDetails";

// import SideBar from "./Components/SideBar";

// import empdetails from "./Pages/EmpDetail";

// import report from "./Pages/EmpReportpage";

// import { AdminProvider } from "./context/adminContext";
// import AddUser from "./Components/Admin/AddUser";
// import Admin from "./Components/Admin/Admin";
// import EditUser from "./Components/Admin/EditUser";
// import ViewUser from "./Components/Admin/ViewUser";

// import Header from "./Components/Header";
// import progress from "./Pages/progresspage";

// import login from "./Pages/LoginPage.js";
import ProgressDetails from "./Components/Employee/ProgressDetails";
import { PrivateRoute } from "./services/PrivateRoute";
import { GlobalContext } from "./context/GlobalContext";
import changePassword from "./Components/Employee/ChangePasswordForm";
import ReportDetails from "./Components/Employee/ReportDetails";
import { ReportProvider } from "./context/ContextReport";
// import {GlobalContext} from "./context/GlobalContext"
// import changePassword from './Components/Employee/ChangePasswordForm'
import ErrorBoundary from "./Components/Error/ErrorBoundary";
import { EmpProvider } from "./context/ContextEmp";
import EditBank from "./Components/Admin/EditBank";
import EditDependent from "./Components/Admin/EditDependent";
import AddBank from "./Components/Admin/AddBank";
import Help from "./Pages/help";
// let LoggedInUser =true
import error from "./Components/Error/Err";
import Err from "./Err";
export default class App extends Component {
  static contextType = GlobalContext;

  // componentDidMount() {
  //   //  const loggedInUser = this.context.loggedInUser;
  //   const LoggedInUser = this.context.LoggedInUser;
  //   // console.log(isUserLoggedIn);
  // }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Switch>
            <div>
              {/*localStorage.getItem('token') && (
             
                  <MovieProvider>
                    <Header />
                    <SideBar />
                  </MovieProvider>
                
             
             )*/}

              <ErrorBoundary>
                <Route exact path="/" component={login} />
                <PrivateRoute exact path="/select" component={Select} />
                <PrivateRoute exact path="/expense" component={ExpenseForm} />
                <PrivateRoute exact path="/medical" component={MedicalForm} />
                <PrivateRoute exact path="/employee" component={empdetails} />
                <PrivateRoute
                  exact
                  path="/employee/edit/:id"
                  component={editProfile}
                />
                <PrivateRoute exact path="/help" component={Help} />
                <PrivateRoute
                  exact
                  path="/employee/changePassword/:id"
                  component={changePassword}
                />
                <ReportProvider>
                  <PrivateRoute exact path="/progress" component={progress} />
                  <PrivateRoute
                    exact
                    path="/report/:id"
                    component={ReportDetails}
                  />

                  <PrivateRoute
                    exact
                    path="/progress/:id"
                    component={ProgressDetails}
                  />
                  <PrivateRoute exact path="/report" component={report} />
                </ReportProvider>
                {/* <PrivateRoute exact path="/report" component={report} /> */}
                <RequestProvider>
                  <PrivateRoute
                    exact
                    path="/techlead/requests"
                    component={Requests}
                  />
                  <PrivateRoute
                    exact
                    path="/techlead/reports"
                    component={Reports}
                  />
                  <PrivateRoute
                    exact
                    path="/techlead/requests/:id"
                    component={RequestDetails}
                  />
                  <PrivateRoute
                    exact
                    path="/techlead/reports/:id"
                    component={ReportsDetails}
                  />

                  {/* <PrivateRoute
                  exact
                  path="/accountant/requests"
                  component={AccRequests}
                />
                <PrivateRoute
                  exact
                  path="/accountant/reports"
                  component={AccReports}
                />
                <PrivateRoute
                  exact
                  path="/accountant/requests/:id"
                  component={AccRequestDetails}
                />
                <PrivateRoute
                  exact
                  path="/accountant/reports/:id"
                  component={AccRequestDetails}
                /> */}
                </RequestProvider>
                <AccountantProvider>
                  <PrivateRoute
                    exact
                    path="/accountant/requests"
                    component={AccountantRequests}
                  />
                  <PrivateRoute
                    exact
                    path="/accountant/reports"
                    component={AccountantReports}
                  />
                  <PrivateRoute
                    exact
                    path="/accountant/requests/:id"
                    component={AccountantRequestDetails}
                  />
                  <PrivateRoute
                    exact
                    path="/accountant/reports/:id"
                    component={AccountantReportsDetails}
                  />
                </AccountantProvider>
                <AdminProvider>
                  <PrivateRoute exact path="/admin" component={Admin} />
                  <PrivateRoute
                    exact
                    path="/admin/add-user"
                    component={AddUser}
                  />
                  <PrivateRoute
                    exact
                    path="/admin/edit-user/:id"
                    component={EditUser}
                  />
                  <PrivateRoute
                    exact
                    path="/admin/add-bank/:id"
                    component={AddBank}
                  />
                  <PrivateRoute
                    exact
                    path="/admin/edit-bank/:id"
                    component={EditBank}
                  />
                  <PrivateRoute
                    exact
                    path="/admin/edit-dependent/:id"
                    component={EditDependent}
                  />
                  <PrivateRoute
                    exact
                    path="/admin/view-user/:id"
                    component={ViewUser}
                  />
                  <PrivateRoute
                    exact
                    path="/admin/add-dependent/:id"
                    component={AddDependent}
                  />
                </AdminProvider>{" "}
                {/*<Route exact path="/error" component={error} />
                <Route render={() => <Redirect to={{pathname: "/error"}} />} />*/}
                {/*   <Route component={Err}/>*/}
              </ErrorBoundary>
            </div>
          </Switch>
        </BrowserRouter>
      </ThemeProvider>
    );
  }
}
