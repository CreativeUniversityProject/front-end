import host from '../context/GlobalVariables'
const request = async (url, method, body) => {
  const token = localStorage.getItem("token");

  const postOptions = {
    method: method,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };

  const result = await fetch(url, postOptions).then((result) => {
    if (result.ok) {
      if (method !== "DELETE") {
        return result.json();
      }
      return result;
    }
    throw result.status;
  });

  return result;
};

export const addFormE = async (EForm) => {
  const url = `${host}/addExpenseClaim`;
  const Eforms = await request(url, "POST", EForm);
  return Eforms;
};
export const addEmail = async (Email) => {
  const url = `${host}/email`;
  const Emails = await request(url, "POST", Email);
  return Emails;
};


export const getExpenseClaims = async (tid) => {
  const url = `${host}/ExpenseClaimFortechlead/${tid}`;

  const expenseClaims = await request(url, "GET");

  return expenseClaims;
};

export const getExpenseClaimsEmp = async (eid) => {
  const url = `${host}/ExpenseClaimForEmployee/${eid}`;

  const expenseClaims = await request(url, "GET");

  return expenseClaims;
};

export const getProject = async (eid) => {
  const url = `${host}/employeeProjects/${eid}`;

  const project = await request(url, "GET");

  return project;
};

export const getAllExpenseClaims = async () => {
  const url = `${host}/allExpenseClaims`;

  const expenseClaims = await request(url, "GET");

  return expenseClaims;
};

export const updateExpenseClaim = async (exclaim) => {
  const url = `${host}/updateExpenseClaimStatus/${exclaim.expenseFormId}`;

  const expenseClaim = await request(url, "PUT", exclaim);

  return expenseClaim;
};
