import host from '../context/GlobalVariables'
const request = async (url, method, body) => {
  const token = localStorage.getItem("token");

  const postOptions = {
    method: method,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };

  const result = await fetch(url, postOptions).then((result) => {
    if (result.ok) {
      if (method !== "DELETE") {
        return result.json();
      }
      return result;
    }
    throw result.status;
  });

  return result;
};
export const getProfileForEmployee = async (eid) => {
  const url = `${host}/ProfilePictureOfEmployee/${eid}`;

  const profiles = await request(url, "GET");

  return profiles;
};
export const addProfile = async (profile) => {
  const url = `${host}/addProfilePicture`;
  const profiles = await request(url, "POST", profile);
  return profiles;
};
export const getProfiles = async () => {
  const url = `${host}/allProfilePictures`;

  const users = await request(url, "GET");

  return users;
};
export const updateProfile = async (prof) => {
  const url = `${host}/updateProfilePicture/${prof.employee.employeeId}`;

  const users = await request(url, "PUT", prof);

  return users;
};
