import host from '../../context/GlobalVariables'
const postRequest = async (url, body) => {
  const postOptions = {
    credentials: "include",
    method: "POST",
    headers: {
      "Access-Control-Expose-Headers": "Authorization",
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };

  const result = await fetch(url, postOptions).then((result) => {
    const token = result.headers.get("Authorization");
   
    token && localStorage.setItem("token", token);

    if (result.ok) return result.json();
    throw result.status;
  });

  return result;
};

export const signUpUser = async (newUser) => {
  const url = `${host}/api/addemployee`;
  const user = await postRequest(url, newUser);
  return user;
};
export const signInUser = async ({ email, password }) => {
  const url = `${host}/api/auth/signIn`;
  const user = await postRequest(url, { email, password });
user && localStorage.setItem('user',JSON.stringify(user))
  return user;
};
