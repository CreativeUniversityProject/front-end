import host from '../context/GlobalVariables'
const request = async (url, method, body) => {
  const token = localStorage.getItem("token");

  const postOptions = {
    method: method,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };

  const result = await fetch(url, postOptions).then((result) => {
    if (result.ok) {
      if (method !== "DELETE") {
        return result.json();
      }
      return result;
    }
    throw result.status;
  });

  return result;
};



export const getAccoutant = async (eid) => {
    const url = `${host}/api/users/employeeAccountants/${eid}`;
  
    const user = await request(url,"GET");
  
    return user;
  }
