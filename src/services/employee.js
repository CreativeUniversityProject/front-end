import { Alert } from 'react-bootstrap';
import host from '../context/GlobalVariables'
const request = async (url, method, body) => {
  const token = localStorage.getItem("token");

  const postOptions = {
    method: method,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };

  const result = await fetch(url, postOptions).then((result) => {
    if (result.ok) {
      if (method !== "DELETE") {
        return result.json();
      }
      return result;
    }
   throw result.status;
  });

  return result;
};


export const getDesignation = async (id) => {
  const url = `${host}/Designation/` + id;

  const designation = await request(url, "GET");

  return designation;
};
export const getDepartment = async (id) => {
  const url = `${host}/Department/`+ id;

  const department = await request(url, "GET");

  return department;
};
export const getUserRole = async (id) => {
  const url = `${host}/Roles/` + id;

  const userRole = await request(url, "GET");

  return userRole;
};
export const getEmployee = async (id) => {
  const url = `${host}/employee/` + id;

  const employee = await request(url, "GET");

  return employee;
};

export const changePassword = async (changedPassword) => {
  const url = `${host}/api/auth/changePassword`;

  const password = await request(url, "POST",changedPassword);

  return password;
};
