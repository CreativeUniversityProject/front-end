import host from "../context/GlobalVariables";
const request = async (url, method, body) => {
  const token = localStorage.getItem("token");

  const postOptions = {
    method: method,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };

  const result = await fetch(url, postOptions).then((result) => {
    if (result.ok) {
      if (method !== "DELETE") {
        return result.json();
      }
      return result;
    }
    throw result.status;
  });

  return result;
};

export const getUsers = async () => {
  const url = `${host}/employee`;

  const users = await request(url, "GET");

  return users;
};

export const updateEmployee = async (user) => {
  const url = `http://localhost:8080/admin/updateEmployee`;

  const users = await request(url, "PUT", user);

  return users;
};
export const deleteEmployee = async (id) => {
  const url = `${host}/admin/deleteEmployee/` + id;

  const users = await request(url, "DELETE");

  return users;
};

export const getDesignations = async () => {
  const url = `${host}/allDesignation`;

  const designations = await request(url, "GET");

  return designations;
};

export const getDepartments = async () => {
  const url = `${host}/allDepartment`;

  const departments = await request(url, "GET");

  return departments;
};

export const getRoles = async () => {
  const url = `${host}/allRoles`;

  const roles = await request(url, "GET");

  return roles;
};

export const addUser = async (user) => {
  const url = `${host}/admin/addemployee`;
  const users = await request(url, "POST", user);
  return users;
};
export const addDependent = async (dependent) => {
  const url = "http://localhost:8080/admin/addDependent";
  const dependents = await request(url, "POST", dependent);
  return dependents;
};

export const getDependentForEmployee = async (eid) => {
  const url = `${host}/dependentForEmployee/${eid}`;

  const designations = await request(url, "GET");

  return designations;
};

export const getDependent = async (id) => {
  const url = `${host}/dependent/${id}`;

  const dependent = await request(url, "GET");

  return dependent;
};

export const updateDependent = async (editeddependent) => {
  const url = `${host}/updateDependent/${editeddependent.dependentId}`;

  const dependent = await request(url, "PUT", editeddependent);

  return dependent;
};

export const editProfile = async (currentProfile) => {
  const url = `${host}/updateEmployeeProfile/${currentProfile.employeeId}`;

  const editedProfile = await request(url, "PUT", currentProfile);

  return editedProfile;
};

export const getEmployeeBankAccount = async (eid) => {
  const url = `${host}/employeeBankAccount/${eid}`;

  const employeeBankAccount = await request(url, "GET");

  return employeeBankAccount;
};

export const addBankAccount = async (newbank) => {
  const url = "http://localhost:8080/addBankAccount";
  const bank = await request(url, "POST", newbank);
  return bank;
};

export const updateEmployeeBankAccount = async (newbank) => {
  const url = `http://localhost:8080/updateBankAccountEmployee/${newbank.accountId}`;

  const bank = await request(url, "PUT", newbank);

  return bank;
};
